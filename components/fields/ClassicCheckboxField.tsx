import { Checkbox, FormControl, FormControlLabel } from '@mui/material';
import React from 'react';
import { Controller } from 'react-hook-form';
import { Tooltip, IconButton } from '@mui/material';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import InfoIcon from '@mui/icons-material/Info';
type Props = {
  name: string;
  label: string;
  disabled?: boolean;
  helperText?: string;
};

const CheckboxField = ({ name, label, disabled, helperText }: Props) => {
  return (
    <Controller
      name={name}
      render={({ field: { onChange, value } }) => (
        <FormControl>
          <FormControlLabel
            control={<Checkbox checked={Boolean(value)} onChange={onChange} />}
            label={
              <>
                {label}
                {helperText && (
                  <Tooltip title={helperText}>
                    <InfoIcon style={{ marginLeft: '1em' }} />
                  </Tooltip>
                )}
              </>
            }
            disabled={disabled}
          />
        </FormControl>
      )}
    />
  );
};

export default CheckboxField;

import React, { useEffect, useState } from 'react';
import { FormControl, FormHelperText, InputLabel } from '@mui/material';
import { styled } from '@mui/material/styles';
import { RegisterOptions, useController, useFormContext } from 'react-hook-form';

const customCKEditorConfig = {
  toolbar: {
    shouldNotGroupWhenFull: true, // Prevents CKEditor from grouping toolbar items
  },
  table: {
    contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells'],
  },
  mediaEmbed: {
    previewsInData: true
  },
  simpleUpload: {
    uploadUrl: '/api/files?inlined',
    withCredentials: true,
    headers: {
      PublicUrl: process.env.NEXT_PUBLIC_URI
    }
  },
  image: {
    toolbar: [
			'imageStyle:inline', 'imageStyle:wrapText', 'imageStyle:breakText', '|',
		  'toggleImageCaption', 'imageTextAlternative'
	]
  }
};

const Label = styled(InputLabel)(({ theme }) => ({
  position: 'static',
  transform: 'none',
  marginBottom: theme.spacing(1),
  '&:focus': {
    color: theme.palette.primary.main,
  },
}));

const StyledEditorWrapper = styled('div')(({ theme }) => ({
  width: '100%',
  '& .ck-editor__editable': {
    minHeight: '200px',
    [theme.breakpoints.down('sm')]: {
      minHeight: '150px',
    },
  },
  // Custom CSS to hide additional parts of the editor on smaller screens
  '& .ck-toolbar': {
    [theme.breakpoints.down('sm')]: {
      display: 'none', // You can adjust which part of the toolbar to hide
    },
  },
}));

type Props = {
  name: string;
  label: string;
  rules?: RegisterOptions;
  helperText?: string;
};

const RichTextEditorField = (props: Props) => {
  const { name, label, rules, helperText } = props;

  const { control } = useFormContext();
  const { field, fieldState } = useController({
    control,
    name,
    rules,
  });

  const [editorLoaded, setEditorLoaded] = useState(false);
  const editorRef = React.useRef<{ CKEditor: any; ClassicEditor: any }>();
  const { CKEditor, ClassicEditor } = editorRef.current || {};

  useEffect(() => {
    const importCKEditor = async () => {
      const { CKEditor } = await import('@ckeditor/ckeditor5-react');
      const { default: editor } = await import('ckeditor5-custom-build/build/ckeditor');
      editorRef.current = { CKEditor, ClassicEditor: editor };
      setEditorLoaded(true);
    };

    importCKEditor();
  }, []);

  useEffect(() => {
    if (editorLoaded) {
      // Trigger a window resize to force CKEditor to adapt
      window.dispatchEvent(new Event('resize'));
    }
  }, [editorLoaded]);

  return (
    <FormControl
      variant="outlined"
      required={rules && Object.keys(rules).includes('required')}
      error={fieldState.invalid}
      fullWidth
    >
      <Label variant="outlined">{label}</Label>
      {editorLoaded && (
        <StyledEditorWrapper>
          <CKEditor
            editor={ClassicEditor}
            config={customCKEditorConfig}
            data={field.value}
            onReady={(editor) => {
              // Manually trigger layout recalculation when the editor is ready
              editor.ui.view.editable.element.style.maxWidth = '100%';
              window.dispatchEvent(new Event('resize')); // Force a resize event
            }}
            onChange={(event, editor) =>
              field.onChange({
                target: {
                  name: name || '',
                  value: editor.getData(),
                },
                type: 'change',
              })
            }
            onBlur={(event, editor) => {
              field.onBlur();
            }}
          />
        </StyledEditorWrapper>
      )}
      <FormHelperText variant="outlined">{fieldState.error?.message || helperText}</FormHelperText>
    </FormControl>
  );
};

export default RichTextEditorField;

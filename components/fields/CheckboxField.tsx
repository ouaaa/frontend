import React from 'react';
import { Checkbox, FormControl, FormControlLabel, FormHelperText } from '@mui/material';
import { useFormContext, useController } from 'react-hook-form';

type Props = {
  name: string;
  label: string;
  disabled?: boolean;
  valueCheckbox?: string;
  onChange?;
};

const CheckboxField = ({ name, label, disabled,checked, valueCheckbox,onChange:onChangeFunction }: Props) => {
  // Get form methods from context
  const { control } = useFormContext();
  
  // Use useController to manage the field state
  const {
    field: { onChange, value = [], onBlur },
    fieldState: { error }
  } = useController({
    name,
    control,
    defaultValue: [] // Ensure default value is an array
  });

  // Determine if the checkbox should be checked
  const isChecked = checked || (Array.isArray(value) ? value.includes(valueCheckbox) : false);
  return (
    <FormControl>
      <FormControlLabel
        control={
          <Checkbox
            checked={isChecked}
            onChange={(e) => {
              const isChecked = e.target.checked;
              const newValue = isChecked
                ? [...(Array.isArray(value) ? value : []), valueCheckbox]
                : (Array.isArray(value) ? value : []).filter((item) => item !== valueCheckbox);

                onChange(newValue);
                if (onChangeFunction && typeof onChangeFunction === 'function') {
                  onChangeFunction(e);
                }
            }}
            onBlur={onBlur}
          />
        }
        label={label}
        disabled={disabled}
      />
      {error && <FormHelperText>{error.message}</FormHelperText>}
    </FormControl>
  );
};

export default CheckboxField;

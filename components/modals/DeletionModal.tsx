import React from 'react';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';

type Props = {
  open: boolean;
  onClose: () => void;
  onSubmit: () => void;
  title: string;
  content: string;
};

const DeletionModal: React.FC<Props> = ({ open, onClose, onSubmit,title,content }) => (
  <Dialog
    open={open}
    onClose={onClose}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
      {content}
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={onClose} color="primary" variant="contained">
        Annuler
      </Button>
      <Button onClick={onSubmit} variant="outlined" color="primary" autoFocus>
        Supprimer
      </Button>
    </DialogActions>
  </Dialog>
);

export default DeletionModal;

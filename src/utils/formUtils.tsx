export interface Entry {
  entryId: number;
  linkDescription: string;
  topSEO: boolean;
}

export function transformToEntriesWithInformation(entriesObject: { [key: string]: string }): Entry[] {
  if (!entriesObject) return [];
  return Object.keys(entriesObject).map(key => {
    // Extract the entryId from the key (e.g., "entries_20" -> 20)
    const entryId = parseInt(key.split('_')[1], 10);
    // Extract the linkDescription from the value of the key
    const linkDescription = entriesObject[key];

    return {
      entryId: entryId,            // Set entryId to the extracted number
      linkDescription: linkDescription,  // Set linkDescription to the value
      topSEO: true                 // topSEO is always true
    };
  });
}

export const transformEntries = (entries: { [key: string]: any }): (number | string)[] => {
  
 const entriesList = Object.keys(entries).reduce<(number | string)[]>((acc, collectionName) => {
    const entryIds = entries[collectionName];

    if (entryIds === undefined) return acc; // Skip undefined entries

    if (typeof entryIds === 'number' || typeof entryIds === 'string') {
      // If entryIds is a single number or string, push it directly
      acc.push(entryIds);
    } else if (Array.isArray(entryIds)) {
      // If entryIds is an array, push all elements
      acc.push(...entryIds);
    } else if (typeof entryIds === 'object' && entryIds !== null) {
      // If entryIds is an object, filter and push keys where the value is true
      acc.push(...Object.keys(entryIds).filter(id => entryIds[id] === true));
    }
    return acc;
  }, []);
debugger;
  return entriesList;
};

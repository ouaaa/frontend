export default {
  'entree': '🥗 Entrée',
  'plat': '🍽️  Plat',
  'dessert': '🍰 Dessert',
  'boisson': '🥤 Boisson',
  'buffet': '🍴 Buffet',

};

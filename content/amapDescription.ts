export const amapDescription: string = 
  "Une AMAP (Association pour le Maintien d’une Agriculture Paysanne) permet de soutenir une agriculture locale, écologique et équitable. En rejoignant une AMAP, vous recevez des produits frais et de saison tout en créant un lien direct avec les producteurs. Un engagement concret pour une alimentation saine et un modèle agricole durable !";

module.exports = {
  printWidth: 120,
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  // arrowParens: 'avoid',
  jsxBracketSameLine: false,
};

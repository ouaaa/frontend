import React, { useCallback, useMemo, useState, useEffect, useRef } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import {
  Stack, IconButton, Tooltip, useMediaQuery, useTheme, CircularProgress, Box, Typography,
} from '@mui/material';
import PrintIcon from '@mui/icons-material/Print';
import DownloadIcon from '@mui/icons-material/Download';
import ActorCard from 'components/cards/ActorCard';
import useExcelExport from '../../../hooks/useExcelExport.ts';

const useStyles = makeStyles((theme) => ({
  '@media print': {
    header: {
      display: 'none !important',
    },
    stack: {
      display: 'block !important',
      '& > *': {
        breakInside: 'avoid',
      },
    },
  },
  actors: {
    width: '100%',
    margin: '0',
    paddingBottom: 66,
    padding: '10px 2em',
    [theme.breakpoints.down('md')]: {
      padding: '0px 0em',
    },
  },
  header: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
  },
  title: {
    color: '#2C367E',
    fontSize: '2.3em',
    display: 'flex',
    flex: 1,
  },
  loading: {
    display: 'flex',
    justifyContent: 'center',
    padding: '20px 0',
  },
  alphabetNav: {
    position: 'fixed',
    [theme.breakpoints.up('sm')]: {
      right: '25px',
      top: '100px', // Adjust to ensure it doesn't overlap with header
    },
    [theme.breakpoints.down('md')]: {
      right: '0px',
      top: '90px', // Adjust to ensure it doesn't overlap with header
    },
  
    alignItems: 'center',
    zIndex: 1000,
    backgroundColor: 'white',
    display: 'none', // Hidden by default
      display: 'flex', // Show only on mobile
      flexDirection: 'column',
  },
  alphabetItem: {
    cursor: 'pointer',
    fontSize: '1.1em',
    width: '30px',
    height: '30px',
    padding: 'px ',
    transition: 'color 0.3s',
    color: 'black', // Default color for letters
    '&:hover': {
      color: 'rgb(217, 101, 82)',
    
      borderRadius: '50%', // Make it a circle
    },
  },
  activeLetter: {
    color: 'rgb(44, 54, 126)', // Active color for selected letter
  },
  activeCursor: {
    position: 'absolute',
    width: '3px', // Width of the vertical cursor
    height: '30px', // Height of the cursor
    color: 'rgb(44, 54, 126)', // Cursor color
    transition: 'top 0.1s',
    borderRadius: '2px',
    zIndex: 999, // Ensure the cursor is above other elements
  },
  actorContainer: {
    scrollMarginTop: '100px', // Adjust this based on your header height
  },
}));

const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');

const Actors = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const exportData = useExcelExport();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  const { data } = props;

  const compare = useCallback((a, b) => {
    return a.name.localeCompare(b.name, undefined, { sensitivity: 'accent' });
  }, []);

  // State for loading actors in chunks
  const [visibleActors, setVisibleActors] = useState(10); // Number of actors to display initially
  const [isLoading, setIsLoading] = useState(false); // Loading state for lazy loading
  const actorRefs = useRef({}); // To store references to each actor element for scrolling
  const [cursorPosition, setCursorPosition] = useState(-1); // For cursor position
  const [dragging, setDragging] = useState(false); // For cursor dragging state
  const [scrollTargetId, setScrollTargetId] = useState(null); // Target actor ID for scrolling
  const [isAlphabetVisible, setIsAlphabetVisible] = useState(matches); // State for alphabet visibility

  // Sorting the data
  const sortedActors = useMemo(() => {
    return data && data.actors.slice().sort(compare);
  }, [data]);

  // Load more actors when scrolling
  const loadMoreActors = useCallback(() => {
    if (isLoading) return; // Prevent multiple triggers
    setIsLoading(true);

    setTimeout(() => {
      setVisibleActors((prev) => Math.min(prev + 10, sortedActors.length)); // Load 10 more actors
      setIsLoading(false);
    }, 10); // Simulate loading time (adjust as needed)
  }, [isLoading, sortedActors.length]);

  // Intersection Observer to load more actors when reaching the bottom
  const observer = useRef();
  const lastActorRef = useCallback(
    (node) => {
      if (isLoading) return;
      if (observer.current) observer.current.disconnect();

      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && visibleActors < sortedActors.length) {
          loadMoreActors(); // Load more actors if we reach the bottom
        }
      });

      if (node) observer.current.observe(node);
    },
    [isLoading, loadMoreActors, visibleActors, sortedActors.length]
  );

  const handleClickPrint = useCallback(() => {
    window.print();
  }, []);

  const handleClickExport = useCallback(() => {
    const actorsToExport = sortedActors.map(actor => ({
      ...actor,
      url: `${process.env.NEXT_PUBLIC_BASE_URL}/acteur/${actor.url}`,
    }));

    exportData({
      data: actorsToExport,
      columns: ['id', 'name', 'address', 'city', 'shortDescription', 'url'],
      columnLabels: ['ID', 'Nom', 'Adresse', 'Ville', 'Description', 'URL'],
      columnOptions: [{ wch: 4 }, { wch: 30 }, { wch: 30 }, { wch: 30 }, { wch: 60 }, { wch: 50 }],
      sheetName: 'acteurs',
      fileName: 'acteurs',
    });
  }, [sortedActors]);

  // Handle scrolling to a specific letter
  const scrollToLetter = useCallback((letter) => {
    const actorsStartingWithLetter = sortedActors.filter(actor => actor.name.toUpperCase().startsWith(letter));
    
    if (actorsStartingWithLetter.length > 0) {
      setVisibleActors(sortedActors.indexOf(actorsStartingWithLetter[0]) + actorsStartingWithLetter.length);
      setScrollTargetId(actorsStartingWithLetter[0].id);
    } else {
      setVisibleActors(sortedActors.length); // Load all actors
      const updatedFirstActor = sortedActors.find(actor => actor.name.toUpperCase().startsWith(letter));
      if (updatedFirstActor) {
        setScrollTargetId(updatedFirstActor.id); // Set the scroll target
      }
    }
  }, [sortedActors]);

  const handleMouseEnter = (index) => {
    setCursorPosition(index);
  };

  const handleMouseLeave = () => {
    setCursorPosition(-1);
  };

  const handleMouseDown = (event) => {
    setDragging(true);
    handleMouseEnter(event.currentTarget.dataset.index);
  };

  const handleMouseMove = (event) => {
    if (dragging) {
      const index = Math.floor((event.clientY - event.currentTarget.getBoundingClientRect().top) / 30);
      if (index >= 0 && index < alphabet.length) {
        setCursorPosition(index);
      }
    }
  };

  const handleMouseUp = () => {
    setDragging(false);
  };
  const handleScroll = () => {
    if (window.scrollY > 200) { // Adjust this threshold as needed
      setIsAlphabetVisible(true);
    } else {
      setIsAlphabetVisible(false);
    }

    // Reset the timeout when scrolling
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }

    // Set a timeout to hide the alphabet after a period of inactivity
    timeoutRef.current = setTimeout(() => {
      setIsAlphabetVisible(false);
    }, 2000); // Adjust the delay as necessary (e.g., 1000ms = 1 second)
  };

  // Create a ref to store the timeout ID
  const timeoutRef = useRef(null);

 // Attach the scroll event listener
 useEffect(() => {
  window.addEventListener('scroll', handleScroll);
  return () => {
    window.removeEventListener('scroll', handleScroll);
    clearTimeout(timeoutRef.current); // Clean up the timeout on unmount
  };
}, []);

  useEffect(() => {
    if (scrollTargetId) {
      const actorElement = actorRefs.current[scrollTargetId];
      if (actorElement) {
        actorElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
      }
      setScrollTargetId(null); // Reset the target after scrolling
    }
  }, [scrollTargetId]);

  useEffect(() => {
    if (cursorPosition >= 0) {
      const letter = alphabet[cursorPosition];
      scrollToLetter(letter); // Scroll to the letter when cursor changes
    }
  }, [cursorPosition, scrollToLetter]);

  return (
    <div className={classes.actors} onMouseMove={handleMouseMove} onMouseUp={handleMouseUp}>
      <div className={classes.header}>
        <h1 className={classes.title}>
          Liste des acteurs
        </h1>
        {matches && ( // Only show export options on non-mobile screens
          <div>
            <Tooltip title="Imprimer">
              <IconButton onClick={handleClickPrint} size="large">
                <PrintIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Exporter">
              <IconButton onClick={handleClickExport} size="large">
                <DownloadIcon />
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>
      <Stack spacing={2} className={classes.stack}>
        {sortedActors.slice(0, visibleActors).map((actor, index) => {
          if (index === visibleActors - 1) {
            return (
              <div ref={lastActorRef} key={actor.id}>
                <ActorCard ref={(el) => (actorRefs.current[actor.id] = el)} key={actor.id} actor={actor} />
              </div>
            );
          }
          return (
            <div key={actor.id} ref={(el) => (actorRefs.current[actor.id] = el)}>
              <ActorCard key={actor.id} actor={actor} />
            </div>
          );
        })}
      </Stack>
      {isLoading && (
        <div className={classes.loading}>
          <CircularProgress />
        </div>
      )}
      {/* Alphabet Navigation */}
      {isAlphabetVisible && ( // Show alphabet only when scrolled down
        <Box className={classes.alphabetNav} onMouseLeave={handleMouseLeave}>
          {alphabet.map((letter, index) => (
            <Typography
              key={letter}
              className={`${classes.alphabetItem} ${cursorPosition === index ? classes.activeLetter : ''}`}
              onClick={() => scrollToLetter(letter)}
              data-index={index}
            >
              {letter}
            </Typography>
          ))}
        </Box>
      )}
      {/* Dragging cursor */}
      {cursorPosition >= 0 && (
        <div
          className={classes.activeCursor}
          style={{
            top: `${cursorPosition * 30 + 100}px`, // Adjust based on height of each letter
          }}
        />
      )}
    </div>
  );
};

export default Actors;

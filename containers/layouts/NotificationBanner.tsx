import React, { useState, useEffect } from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CssBaseline from '@mui/material/CssBaseline';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import { styled } from '@mui/material/styles';
import Slide from '@mui/material/Slide';
import { Box, Typography, Button, Link } from '@mui/material';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { useQuery, gql } from '@apollo/client';

const GET_BANNER_NOTIFICATION = gql`
  query GetBannerNotification($code: String!) {
    entry(code: $code) {
      id
      label
      description
      position
      url
      enabled
    }
  }
`;

interface Props {
  window?: () => Window;
  children?: React.ReactElement;
}

function HideOnScroll(props: Props) {
  const { children, window } = props;

  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
  });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children ?? <div />}
    </Slide>
  );
}

const TextTypography = styled(Typography)(({ theme }) => ({
  color: 'white',
  '&:hover': {
    textDecoration: 'none',
  },
}));

const CustomLink = styled(Link)(({ theme }) => ({
  color: 'white',
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  textDecoration: 'none',
  position: 'relative',
  '&:hover .arrow': {
    transform: 'translateX(10px)', // Move the arrow slightly to the right
  },
}));

const ArrowBox = styled(Box)(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  fontSize: '1.5rem',
  color: 'white',
  transition: 'transform 0.3s ease',
  '@media (max-width:600px)': {
    display: 'none', // Hide on mobile devices
  },
}));

const NotificationBanner: React.FC = () => {
  const [isVisible, setIsVisible] = useState(false);

  // Fetch the notification data from GraphQL
  const { loading, error, data } = useQuery(GET_BANNER_NOTIFICATION, {
    variables: { code: 'bannerNotification' },
  });

  useEffect(() => {
    if (loading) return;
    const position = data?.entry?.position;

    const bannerClosed = localStorage.getItem(`ouaaaBannerClosed${position}`);
    if (!bannerClosed) {
      setIsVisible(true);
    }
  }, [loading, data]);

  const closeBanner = () => {
    const position = data?.entry?.position;
    localStorage.setItem(`ouaaaBannerClosed${position}`, 'true');
    setIsVisible(false);
  };

      if (loading || !data?.entry || !data.entry.enabled || !isVisible) {
    return null; // Avoid rendering if data is not loaded or entry is missing
  }

  return (
    <>
      <CssBaseline />
      <HideOnScroll>
        <AppBar position="sticky" sx={{ backgroundColor: '#f0a300', boxShadow: 2 }}>
          <Toolbar>
            <CustomLink href={data.entry.url} onClick={closeBanner}>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: 1,
                  justifyContent: 'space-between',
                  width: '100%',
                }}
              >
                <TextTypography variant="body1">
                  <strong>{data.entry.label}</strong> {data.entry.description}
                </TextTypography>
                <ArrowBox className="arrow">
                  <ArrowForwardIcon />
                </ArrowBox>
                <Box/>
              </Box>
            </CustomLink>
            <Button
              onClick={closeBanner}
              sx={{ color: 'white', fontWeight: 'bold', fontSize: '1rem' }}
            >
              ✕
            </Button>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
    </>
  );
};

export default NotificationBanner;

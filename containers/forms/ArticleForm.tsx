/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { Box, Button, Stack, TextField } from '@mui/material';
import { useForm, SubmitHandler, FormProvider } from 'react-hook-form';
import RichTextEditorField from '../../components/fields/RichTextEditorField';
import ImageUploadField, { FileType, isExistingFile } from '../../components/fields/ImageUploadField';
import TextInputField from 'components/fields/TextInputField';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/client';
import AutocompleteField from 'components/fields/AutocompleteField';

const GET_ACTORS = gql`
  query Actors {
    actors(canAdmin: true) {
      id
      name
    }
  }
`;

type GET_ACTORS_TYPE = {
  actors: {
    id: string;
    name: string;
  }[];
};

export type ArticleFields = {
  actor: string;
  label: string;
  shortDescription: string;
  mainPicture: FileType[];
  content: string;
  actors: string[];
  pictures: FileType[];
};

type Props = {
  submitLabel: string;
  onSubmit: SubmitHandler<ArticleFields>;
  defaultValues?: ArticleFields,
  disabledFields?: (keyof ArticleFields)[];
};

const ArticleForm = ({ defaultValues, submitLabel, onSubmit,disabledFields  }: Props) => {
  const { data } = useQuery<GET_ACTORS_TYPE>(GET_ACTORS);
  const form = useForm<ArticleFields>({
    mode: 'onTouched',
    defaultValues,
  });


  const {
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = form;
  const actorId = watch('actor');
  
  const errorMessages = Object.values(errors).map((e) => e.message);

  useEffect(() => {
    if (data?.actors) {
      if (data?.actors.length === 1) {
        setValue('actor', data?.actors[0].id);
      }
    }
  }, [actorId, data]);
  return (
    <FormProvider {...form}>
      <Box component="form" onSubmit={handleSubmit(onSubmit)} sx={{ my: 3 }}>
        <Stack direction="column" gap={2}>
        <AutocompleteField
            name="actor"
            label="Acteur créateur de l'article"
            options={(data?.actors || []).map((s) => ({ id: s.id, label: s.name }))}
            rules={{
              required: 'Acteur requis',
            }}
            disabled={disabledFields?.includes('actor')}
          />

        
          <TextInputField
            name="label"
            label="Nom de l'article"
            rules={{
              required: "Nom de l'article requis",
            }}
          />

          <TextInputField
            name="shortDescription"
            label="Résumé"
            rules={{
              required: 'Résumé requis',
              maxLength: { value: 90, message: 'Maximum 90 caractères' },
            }}
          />

          <ImageUploadField
            name="mainPicture"
            label="Bannière"
            filesLimit={1}
            rules={{
              validate: {
                required: (pictures) =>
                  pictures?.filter((picture) => !(isExistingFile(picture) && picture.deleted)).length > 0 ||
                  'Bannière requise',
              },
            }}
          />

          <RichTextEditorField
            name="content"
            label="Contenu de l'article"
            rules={{
              required: "Contenu de l'article requis",
            }}
          />

          <ImageUploadField name="pictures" label="Autres photos" />

          <AutocompleteField
            name="actors"
            label="Acteur(s) associé(s) à l'article"
            multiple
            options={(data?.actors || []).map((s) => ({ id: s.id, label: s.name }))}
          />

          <Button type="submit" variant="contained" sx={{ margin: 'auto' }}>
            {submitLabel}
          </Button>
        </Stack>
      </Box>
    </FormProvider>
  );
};

export default ArticleForm;

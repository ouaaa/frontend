import * as React from "react";

export interface EntriesProps {
  children: JSX.Element;
  initValues: Array<number>;
  onAddCheckedCheckbox?: (item: number) => void;
  onRemoveCheckedCheckbox?: (item: number) => void;
}

export const EntriesContext = React.createContext({});

const Entries: React.FunctionComponent<EntriesProps> = ({
  children,
  initValues,
  onAddCheckedCheckbox,
  onRemoveCheckedCheckbox,
}) => {
  const [checkedCheckboxes, setCheckedCheckboxes] = React.useState<Array<number>>(initValues ?? []);

  const addCheckedCheckbox = React.useCallback(
    (item: number) => {
      if (!checkedCheckboxes.includes(item)) {
        const newCheckedCheckboxes = [...checkedCheckboxes, item];
        setCheckedCheckboxes(newCheckedCheckboxes);
        onAddCheckedCheckbox?.(item); // Appelle la callback si elle est définie
        return newCheckedCheckboxes.length >= 3;
      }
      return false;
    },
    [checkedCheckboxes, onAddCheckedCheckbox]
  );

  const removeCheckedCheckbox = React.useCallback(
    (item: number) => {
      if (checkedCheckboxes.includes(item)) {
        const newCheckedCheckboxes = checkedCheckboxes.filter((checkbox) => checkbox !== item);
        setCheckedCheckboxes(newCheckedCheckboxes);
        onRemoveCheckedCheckbox?.(item); // Appelle la callback si elle est définie
      }
    },
    [checkedCheckboxes, onRemoveCheckedCheckbox]
  );

  const getList = React.useCallback(() => checkedCheckboxes, [checkedCheckboxes]);

  return (
    <EntriesContext.Provider
      value={{
        addCheckedCheckbox,
        removeCheckedCheckbox,
        getList,
      }}
    >
      {children}
    </EntriesContext.Provider>
  );
};

export default Entries;

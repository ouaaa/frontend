/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState, useMemo,useCallback,ChangeEvent } from 'react';
import { Box, Button, Stack, TextField,Typography } from '@mui/material';
import { useForm, SubmitHandler, FormProvider } from 'react-hook-form';
import RichTextEditorField from '../../components/fields/RichTextEditorField';
import ImageUploadField, { FileType, isExistingFile } from '../../components/fields/ImageUploadField';
import TextInputField from 'components/fields/TextInputField';
import gql from 'graphql-tag';
import { set } from 'lodash';
import { useQuery } from '@apollo/client';
import AutocompleteField from 'components/fields/AutocompleteField';
import { IngredientItem } from 'components/IngredientItem';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { Ingredient } from 'components/IngredientItem';
import recipeCategories from 'src/recipeCategories';
import SelectField from 'components/fields/SelectField';
const styles = {
  container: {
    textAlign: 'center',
    width: '75%',
  },
  field: {
    marginBottom: (theme) => theme.spacing(3),
  },
  label: {
    fontWeight: 600,
  },
  submit: {
    margin: (theme) => theme.spacing(3, 0, 2),
  },
}

const GET_ACTORS = gql`
  query Actors {
    actors(canAdmin: true) {
      id
      name
    }
  }
`;

type Ingredient = {
  id?: number,
  name: string,
  unit: string,
  quantity: number,
  baseAlimIngredientId: number,
  description: string,
}

type GET_ACTORS_TYPE = {
  actors: {
    id: string;
    name: string;
  }[];
};
const GET_INGREDIENTBASEALIM = gql`
query ingredientBaseAlim {
  ingredientBaseAlim {
    id
    produit
    poids
    energie
    proteines
    lipides
    glucides
    empreinteCarbone
    agriculture
    transformation
    emballage
    transport
    distribution
    consommation
    poidsParUnite
    densite
    poidsParCuillereASoupe
    poidsParCuillereACafe
  }
}
`;
export type RecipeFields = {
  actor: string;
  category: string;
  label: string;
  shortDescription: string;
  mainPicture: FileType[];
  content: string;
  actors: string[];
  pictures: FileType[];
  ingredients: Ingredient[];
  nbPerson: String;
};

type Props = {
  submitLabel: string;
  onSubmit: SubmitHandler<RecipeFields>;
  defaultValues?: RecipeFields,
  disabledFields?: (keyof RecipeFields)[];
};


const RecipeForm = ({ defaultValues, submitLabel, onSubmit,disabledFields  }: Props) => {
  const { data } = useQuery<GET_ACTORS_TYPE>(GET_ACTORS);
  const { data: dataIngredientBaseAlim } = useQuery(GET_INGREDIENTBASEALIM, {});
  const form = useForm<RecipeFields>({
    mode: 'onTouched',
    defaultValues,
  });


  const {
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = form;
  const actorId = watch('actor');
  
  const ingredients: Ingredient[] =  watch('ingredients');
  

  const errorMessages = Object.values(errors).map((e) => e.message);

  const handleChangeIngredient = useCallback((event: ChangeEvent, index: number) => {
    const values = {
      ingredients: [...ingredients],
    };
    set(values, event.target.name, event.target.value);

    setValue('ingredients',values.ingredients);
  }, [ingredients]);

  const handleRemoveIngredient = useCallback((event: ChangeEvent, index: number) => {
    const nameToRemove = event.target.name;
    const values = ingredients.slice(0, index).concat(ingredients.slice(index + 1));
    
    setValue('ingredients', values);
  }, [ingredients]);

  useEffect(() => {
    if (data?.actors) {
      if (data?.actors.length === 1) {
        setValue('actor', data?.actors[0].id);
      }
    }
  }, [actorId, data]);
  return (
    <FormProvider {...form}>
      <Box component="form" onSubmit={handleSubmit(onSubmit)} sx={{ my: 3 }}>
        <Stack direction="column" gap={2}>
        {data?.actors && data?.actors.length !== 0 && (
        <AutocompleteField
            name="actor"
            label="Acteur créateur de la recette (optionnel)"
            options={(data?.actors || []).map((s) => ({ id: s.id, label: s.name }))}
            disabled={disabledFields?.includes('actor')}
          />
        )}

        <SelectField
            name="category"
            label="Catégorie"
            options={Object.entries(recipeCategories).map(([value, label]) => ({ value, label }))}
            rules={{
              required: 'Catégorie requise',
            }}
          />

          <TextInputField
            name="label"
            label="Nom de la recette"
            rules={{
              required: "Nom de la recette requis",
            }}
          />

          <TextInputField
            name="nbPerson"
            label="Nombre de personnes"
            type='number'
            rules={{
              min: { value: 1, message: 'Le nombre de personnes doit être supérieur à 0' },
            }}
          />
          <TextInputField
            name="shortDescription"
            label="Description de la recette"
            rules={{
              required: 'Description de la recette requie',
              maxLength: { value: 90, message: 'Maximum 90 caractères' },
            }}
          />

          <ImageUploadField
            name="mainPicture"
            label="Photo principale"
            filesLimit={1}
            rules={{
              validate: {
          required: (pictures) =>
            pictures?.filter((picture) => !(isExistingFile(picture) && picture.deleted)).length > 0 ||
            'Photo principale requise',
              },
            }}
          />
        <Typography variant="body1" color="primary" sx={styles.label}>
          Ingrédients
        </Typography>
        <div>
          {
            dataIngredientBaseAlim && ingredients.map((ingredient: Ingredient, index: number) => {
             return ( <IngredientItem
              key={index}
              ingredient={ingredient}
              index={index}
              dataIngredientBaseAlim = {dataIngredientBaseAlim}
              handleChangeIngredient={handleChangeIngredient}
              handleDeleteIngredient={handleRemoveIngredient}
            />
              )
            })
          }
          <Button onClick={() => {
            const values = [...ingredients];
            values.push({ id: undefined, unit: '', quantity: 0, name: '', baseAlimIngredientId: 0, description: '' });
            setValue('ingredients', values);
          }}>

            <AddCircleOutlineIcon sx={{ marginRight: '5px' }} />
            Ajouter un ingrédient
          </Button>
        </div>


    
          <RichTextEditorField
            name="content"
            label="Etapes de la recette"
            rules={{
              required: "Etapes de la recette requises",
            }}
          />

<ImageUploadField
            name="pictures"
            label="Autres photos"
            dropzoneText="Déposez ici vos autres photos au format jpg et de poids inférieur à 4Mo"
          />


          <Button type="submit" variant="contained" sx={{ margin: 'auto' }}>
            {submitLabel}
          </Button>
        </Stack>
      </Box>
    </FormProvider>
  );
};

export default RecipeForm;

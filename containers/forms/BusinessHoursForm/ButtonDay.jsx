import React, { useEffect } from 'react';
import Button from '@mui/material/Button';
import { withStyles } from '@mui/styles';

const StyledButton = withStyles({
  root: {
    borderRadius: 1,
    border: 0,
    padding: '0 0 !important',
    margin: '2px!important',
    minWidth: '20px!important',
    borderRadius: '20px!important',
  },
  label: {
    textTransform: 'capitalize',
  },
})(Button);

export default function ButtonDay(props) {
  const { text, selectDays, selected, dayId, alreadySelected } = props;
  const [flag, setFlag] = React.useState(selected);

  // console.log("flag", flag);

  const handleClick = (e) => {
    setFlag(!flag);
    selectDays(e);

  };

  return (
    <StyledButton
      onClick={handleClick}
      variant="contained"
      style={{ backgroundColor: flag ? '#2C367E' : 'white', color: flag ? 'white' : '#2C367E' }}
      data-id={dayId}
      disabled={alreadySelected}
    >
      {text}
    </StyledButton>
  );
}

/* eslint-disable react/jsx-props-no-spreading */
import React, { useCallback, useEffect, useState } from 'react';
import { Box, Checkbox, Stack, Tooltip, Typography } from '@mui/material';
import { useForm, SubmitHandler, FormProvider } from 'react-hook-form';
import RichTextEditorField from '../../components/fields/RichTextEditorField';
import ImageUploadField, { FileType } from 'components/fields/ImageUploadField';
import GooglePlacesField, { LocationType } from '../../components/fields/GooglePlacesField';
import DateTimePickerField from '../../components/fields/DateTimePickerField';
import LoadingButton from '@mui/lab/LoadingButton';
import makeStyles from '@mui/styles/makeStyles';
import gql from 'graphql-tag';
import { useQuery, useLazyQuery } from '@apollo/client';
import RecurringEventInput from 'components/form/recurringEventInput/RecurringEventInput';
import AutocompleteField from 'components/fields/AutocompleteField';
import TextInputField from 'components/fields/TextInputField';
import UrlInputField from 'components/fields/UrlInputField';
import CheckboxField from 'components/fields/CheckboxField';
import ClassicCheckboxField from 'components/fields/ClassicCheckboxField';
import InfoIcon from '@mui/icons-material/Info';
import Entries from './Entries';
import { TreeView } from '@mui/x-tree-view/TreeView';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import { styled } from '@mui/material/styles';
import StyledTreeItem from '../../components/filters/StyledTreeItem';
import RadioGroupForContext from './RadioGroupForContext';
import FormControlLabel from '@mui/material/FormControlLabel';
import RadioField from 'components/fields/RadioField';

const GET_ACTORS = gql`
  query Actors {
    actors(canAdmin: true) {
      id
      name
    }
  }
`;

const GET_ALL_ACTORS = gql`
  query Actors {
    actors {
      id
      name
    }
  }
`;
const GET_EVENTS = gql`
query events ($notFinished: Boolean ) {
  events (notFinished: $notFinished){
    id
    label
    startedAt
    endedAt
    url
  }
}
`;

type GET_ACTORS_TYPE = {
  actors: {
    id: string;
    name: string;
  }[];
};
type GET_EVENTS_TYPE = {
  events: {
    id: string;
    label: string;
  }[];
};
export type EventFields = {
  actor: string;
  label: string;
  address: LocationType;
  startedAt: string | null;
  endedAt: string | null;
  shortDescription: string;
  description: string;
  facebookUrl: string;
  practicalInfo: string;
  logoPicture: FileType[];
  mainPicture: FileType[];
  pictures: FileType[];
  participateButton: boolean;
  entries: {
    [collectionName: string]: {
      [entryId: string]: boolean;
    };
  };
  proposeEvent: boolean;
  actors: [String];
  inviteActors: string[];
  referencingActor: string;
  emailInvitationText: string;
  parentEvent: string;
  dateRule: string;
  enabledRegistration : boolean;
  registerLink : String;
  limitPlace : number;
  entriesList: { id: string; }[];
  entriesWithInformation: { entryId: number; linkDescription: string; topSEO: boolean; }[];

};

type Props = {
  submitLabel: string;
  onSubmit: SubmitHandler<EventFields>;
  loading?: boolean;
  defaultValues?: Partial<EventFields>;
  additionalButton?: JSX.Element;
  disabledFields?: (keyof EventFields)[];
};

const useStyles = makeStyles((theme) => ({
  field: {
    marginBottom: theme.spacing(3),
  },
  datetime: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  categories: {
    '& span': {
      fontWeight: '100',
    },
  },
  location: {
    margin: '1em 0',
    '& input': {
      height: '3.5em',
      borderRadius: '4px',
      boxShadow: 'none',
      border: 'solid 1px lightgray',
      fontFamily: 'Roboto',
      fontSize: '16px',
      width: '100%',
      '&:hover': {
        border: 'solid 1px lightgray',
      },
      '&:focus': {
        border: 'solid 1px lightgray',
      },
      '&:active': {
        border: 'solid 1px lightgray',
      },
    },
  },
  label: {
    fontWeight: 600,
  },
  collectionLabel: {
    textAlign: 'center',
    color: '#2C367E',
    fontWeight: 600,
  },
  errorautocomplete: {
    border: 'solid 1px #ff1744!important',
    color: '#ff1744!important',
    paddingLeft: '15px',
  },
  justify: {
    textAlign: 'justify',
  },
  rootTree: {
    color: theme.palette.text.secondary,
    '&:hover > $content': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:focus > $content, &$selected > $content': {
      backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
      color: 'var(--tree-view-color)',
    },
    '&:focus > $content $label, &:hover > $content $label, &$selected > $content $label': {
      backgroundColor: 'transparent',
    },
  },
  treeParent: {
    border: '1px solid #ccc!important',
    padding: '5px 0 5px 0',
    width: '100%',
  },
  checkbox: {
    padding: '0px!important',
  },
  container: {
    textAlign: 'center',
  },
  tooltip: {
    marginLeft: '10px',
  },
}));
const GET_COLLECTIONS = gql`
  {
    collections {
      id
      code
      label
      multipleSelection
      position
      actor
      event
      entries {
        id
        label
        icon
        color
        description
        subEntries {
          id
          label
          icon
          description
        }
      }
    }
  }
`;

const EventForm = ({ defaultValues, submitLabel, onSubmit, loading, additionalButton, disabledFields }: Props) => {
  const form = useForm<EventFields>({
    mode: 'onTouched',
    defaultValues,
  });

  const {
    handleSubmit,
    watch,
    setValue,
    formState: { errors },
  } = form;
  const [dataCollections, setDataCollections] = useState({});
  const { loading: loadingCollections, error: errorCollections } = useQuery(
    GET_COLLECTIONS,
    {
      fetchPolicy: 'network-only',
      onCompleted: (datac) => {
        setDataCollections(datac);
      },
    },
  );

  const { data } = useQuery<GET_ACTORS_TYPE>(GET_ACTORS);
  const [loadAllActors, { data: allActorsData }] = useLazyQuery<GET_ACTORS_TYPE>(GET_ALL_ACTORS);

  const [loadAllEvents, { data: allEventsData } ] = useLazyQuery<GET_EVENTS_TYPE>(GET_EVENTS, {
    variables: {
      notFinished: true,
    },
  });

  const styles = useStyles();
  const actorId = watch('actor');
  const parentEvent = watch('parentEvent');
  const proposeEvent = watch('proposeEvent');
  const startedAt = watch('startedAt');
  const actors = watch('actors');
  const entries = watch('entries');
  const dateRule = watch('dateRule');
  const RootTree = styled(TreeView)(({ theme }) => ({
    color: theme.palette.text.secondary,
    '&:thover > $content': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:focus > $content, &$selected > $content': {
      backgroundColor: `var(--tree-view-bg-color, ${theme.palette.grey[400]})`,
      color: 'var(--tree-view-color)',
    },
    '&:focus > $content $label, &:hover > $content $label, &$selected > $content $label': {
      backgroundColor: 'transparent',
    },
  }));

  const [inviteActors, setInviteActors] = useState(false);
  const [hasParentEvent, setHasParentEvent] = useState(parentEvent!==undefined);
  const [showParticipatedActor, setShowParticipatedActor] = useState(actors?.length > 1);
  const [emailText, setEmailText] = useState(
    `Bonjour,\n\n Nous souhaitons vous convier à un événement. Souhaitez vous y participer ? \n Merci de votre réponse par retour d'email. \n Bonne journée`
  );
  setValue('emailInvitationText', emailText);

  const handleChangeDateRule = useCallback((rule) => {
    setValue('dateRule', rule);
  }, []);

  const handleEmailTextChange = (event) => {
    setEmailText(event.target.value);
    setValue('emailInvitationText', event.target.value);
  };

  const errorMessages = Object.values(errors).map((e) => e.message);

  const [actorStartedAt, setActorStartedAt] = useState<Date>(new Date());

  useEffect(() => {
    if (data?.actors) {
      if (data?.actors.length === 1) {
        setValue('actor', data?.actors[0].id);
      }
    }
  }, [actorId, data]);


  useEffect(() => {
    if ((inviteActors || showParticipatedActor) && allActorsData===undefined) {
      loadAllActors();
    }
  }, [inviteActors,showParticipatedActor, loadAllActors]);

  useEffect(() => {
    if ((hasParentEvent) && allEventsData===undefined) {
      loadAllEvents();
    }
  }, [hasParentEvent, loadAllEvents]);

  const isTree = (collection) => {
    let isTree = false;
    if (collection.entries) {
      collection.entries.map((entry) => {
        if (entry.subEntries) {
          entry.subEntries.map((subentry) => {
            isTree = true;
            return isTree;
          });
        }
      });
    }
    return isTree;
  };

  if((data?.actors && data?.actors.length === 0) ){
    return (
      <Typography variant="h6" color="error" align="center">
        Vous n'avez pas encore créé d'acteur. Veuillez en créer un avant de créer un événement.
      </Typography>
    );
  }
  return (
    <FormProvider {...form}>
      <Box component="form" onSubmit={handleSubmit(onSubmit)} sx={{ my: 3 }}>
        <Stack direction="column" gap={3}>
          <AutocompleteField
            name="actor"
            label="Acteur"
            options={(data?.actors || []).map((s) => ({ id: s.id, label: s.name }))}
            rules={{
              required: 'Acteur requis',
            }}
            disabled={disabledFields?.includes('actor')}
          />

          <TextInputField
            name="label"
            label="Nom de l'événement"
            rules={{
              required: "Nom de l'événement requis",
            }}
          />

          <GooglePlacesField
            name="address"
            label="Adresse de l'événement"
            noOptionsText="Aucune adresse trouvée"
            rules={{ required: 'Adresse requise' }}
          />

          <Stack direction={{ xs: 'column', md: 'row' }} spacing={2}>
            <DateTimePickerField
              label="Date de début"
              name="startedAt"
              rules={{ required: 'Date de début requise' }}
              minDateTime={actorStartedAt}
              defaultCalendarMonth={actorStartedAt}
            />
            <DateTimePickerField
              label="Date de fin"
              name="endedAt"
              rules={{
                required: 'Date de fin requise',
                validate: {
                  afterStartDate: (v, formValues) => {
                    return v > formValues.startedAt || 'La date de fin doit être après celle de début';
                  },
                },
              }}
              minDateTime={startedAt ? new Date(startedAt) : undefined}
              defaultCalendarMonth={startedAt ? new Date(startedAt) : actorStartedAt}
            />
          </Stack>
          <RecurringEventInput onChange={handleChangeDateRule} value={dateRule} startDate={startedAt} />
      

          <TextInputField
            name="shortDescription"
            label="Résumé de l'événement"
            rules={{
              required: "Résumé de l'événement requis",
              maxLength: { value: 90, message: 'Maximum 90 caractères' },
            }}
          />

          <RichTextEditorField
            name="description"
            label="Description"
            rules={{
              required: "Description de l'événement requise",
            }}
          />

          <TextInputField
            name="facebookUrl"
            label="Lien externe de l'action (Facebook ou site)"
            sx={{ marginTop: 6 }}
          />

      
            <ClassicCheckboxField
            name="participateButton"
            label="Activer l'inscription à l'événement"
            helperText="Vous recevez les inscriptions par email, et elles seront également visibles dans l'interface d'administration si aucun lien d'inscription n'est configuré. Vous pouvez également définir une limite de taille pour les inscriptions. "
            disabled={disabledFields?.includes('participateButton')}
            />
            {watch('participateButton') && (
            <>
              <UrlInputField
              name="registerLink"
              label="Lien d'inscription (Optionnel)"
              />
              <TextInputField
              name="limitPlace"
              label="Nombre de places limité (Optionnel)"
              type="number"
              rules={{
                min: { value: 1, message: 'Le nombre de places doit être supérieur à 0' },
              }}
              />
            </>
            )}

          <RichTextEditorField
            name="practicalInfo"
            label="Informations pratiques"
          />

          {dataCollections.collections && dataCollections.collections.map((collection) => {
            if (!collection.event) return '';
            if (collection.code === 'larochelle_quarter') return '';
            let { label } = collection;
            let helperText = '';
            if (collection.code === 'event_type') {
              label = "Type d'événement * (choix unique)";
              helperText = 'attention si vous proposez un événement dans le cadre d’un festival, coché ici le type d’événement et non la case festival. Vous pourrez plus loin dans le formulaire rattaché votre événement au festival dans laquelle il s’inclue';
            } else if (collection.code === 'category') {
              label = "Catégorie de l'événement";
              helperText = 'un événement peut traiter un sous-sujet non associé au départ avec la page acteur. Vous pouvez choisir plusieurs sujets à rattacher à votre événement';
            } else if (collection.code === 'event_public_target') {
              helperText = 'contrairement à votre page acteur, ici vous pouvez ajouter plusieurs catégories de publics pour un même événement';
            }

            if (collection.code === 'event_price') return '';

               // Helper function to determine if a subEntry is checked
               function isChecked(defaultValues, id) {
                return Boolean(defaultValues?.entriesList?.some((entry) => entry.id === id));
              }

            const getExpandedNodes = (collection) => {
              const expandedNodes = [];
              collection.entries?.forEach((entry) => {
                if (
                  entry.subEntries?.some((subEntry) => isChecked(defaultValues, subEntry.id))
                ) {
                  expandedNodes.push(entry.id); // Expand the parent if any subEntry is checked
                }
              });
              return expandedNodes;
              
            };

            return (
              <div>
                <br />
                <Typography className={styles.collectionLabel}>
                  {label}
                  {' '}
                  {helperText !== '' && (
                    <Tooltip title={helperText} className={styles.tooltip}>
                      <InfoIcon />
                    </Tooltip>
                  )}
                </Typography>
                <br />
                {isTree(collection) && collection.multipleSelection && (
                
                  <RootTree
                    className={styles.rootTree}
                    defaultCollapseIcon={<ArrowDropDownIcon />}
                    defaultExpandIcon={<ArrowRightIcon />}
                    defaultEndIcon={<div style={{ width: 24 }} />}
                    defaultExpanded={getExpandedNodes(collection)} // Pass expanded nodes
                  >
                    {collection.entries && collection.entries.map((entry) => (
                      <StyledTreeItem
                        key={entry.id}
                        nodeId={entry.id}
                        labelText={entry.label}
                        description={entry.description}
                        hideCheckBox
                        isForm
                        isParent
                        hasSubEntries={entry.subEntries && entry.subEntries.length > 0}
                        className={styles.treeParent}
                      >
                                {entry.subEntries
                                  && entry.subEntries.map((subEntry) => {
                            function ischecked(defaultValues: Partial<EventFields> | undefined, id: any,collection: any): boolean | null | undefined {
                              let e = Boolean(defaultValues?.entriesList?.filter((entry) => entry.id === id).length > 0);
                              return e;
                            }

                            return (
                          <StyledTreeItem
                          key={subEntry.id}
                          nodeId={subEntry.id}
                          collection={collection}
                          labelText={subEntry.label}
                          description={subEntry.description}
                          icon={subEntry.icon}
                          color={entry.color}
                          isForm
                            />
                          );
                        })}
                      </StyledTreeItem>
                    ))}
                  </RootTree>
          
                )}
                {isTree(collection) && !collection.multipleSelection && (
                  <Entries initValues={defaultValues?.entriesList}>
                    <RadioGroupForContext initValue={' '}>
                      <RootTree
                        className={styles.rootTree}
                        defaultCollapseIcon={<ArrowDropDownIcon />}
                        defaultExpandIcon={<ArrowRightIcon />}
                        defaultEndIcon={<div style={{ width: 24 }} />}
                        defaultExpanded={getExpandedNodes(collection)} // Pass expanded nodes
                      >
                        {collection.entries && collection.entries.map((entry) => (
                          <StyledTreeItem
                            key={entry.id}
                            nodeId={entry.id}
                            labelText={entry.label}
                            hideCheckBox
                            isForm
                            hasSubEntries={entry.subEntries && entry.subEntries.length > 0}
                            isParent
                            className={styles.treeParent}
                          >
                            <RadioField
                              name={`entries.${collection.id}`}
                              options={entry.subEntries && entry.subEntries.map(entry => ({ value: entry.id, label: entry.label }))}
                              row={true}
                            />
                          </StyledTreeItem>
                        ))}
                      </RootTree>
                    </RadioGroupForContext>
                  </Entries>
                )}
                {!isTree(collection) && collection.multipleSelection && (
                  <RadioField
                    name={`entries.${collection.id}`}
                    options={collection.entries && collection.entries.map(entry => ({ value: entry.id, label: entry.label }))}
                    row={true}
                  />
                )}
                {!isTree(collection) && !collection.multipleSelection && (
                  <Stack direction={{ xs: 'column', md: 'column' }} spacing={2} justifyContent="center">
                    {collection.entries && collection.entries.map((entry) => (
                      <CheckboxField
                        key={entry.id}
                        name={`entries.${collection.id}`}
                        label={entry.label}
                        valueCheckbox={entry.id}
                      />
                    ))}
                  </Stack>
                )}
              </div>
            );
          })}

          <ImageUploadField
            name="logoPicture"
            label="Logo"
            filesLimit={1}
            dropzoneText="Déposez ici votre logo au format jpg et de poids inférieur à 4Mo"
            sx={{ marginTop: 6 }}
          />

          <ImageUploadField
            name="mainPicture"
            label="Photo principale"
            filesLimit={1}
            dropzoneText="Déposez ici votre photo principale au format jpg et de poids inférieur à 4Mo"
            sx={{ marginTop: 6 }}
            helperText="Merci de charger ici une image d’illustration représentative de l’évènement/ action qui nous permettra de le / la  relayer sur les réseaux sociaux et apparaitra sur les publications."
          />

          <ImageUploadField
            name="pictures"
            label="Autres photos"
            dropzoneText="Déposez ici vos autres photos au format jpg et de poids inférieur à 4Mo"
          />

          <FormControlLabel
            control={<Checkbox  onChange={(event: React.ChangeEvent<HTMLInputElement>) => setInviteActors(event.target.checked)} />}
            label="Inviter des acteurs à participer à l'événement"
          />
          {inviteActors && (
            <>
              <AutocompleteField
                name="inviteActors"
                label="Acteurs à inviter"
                options={(allActorsData?.actors ?? []).map((s) => ({ id: s.id, label: s.name }))}
                multiple
            
              />
              <TextInputField
                name="emailInvitationText"
                label="Texte de l'email d'invitation"
                multiline
                rows={4}
                value={emailText}
                onChange={handleEmailTextChange}
              />
            </>
          )}
          {actors?.length <2 || actors === undefined && (
            <FormControlLabel
                control={<Checkbox onChange={(event: React.ChangeEvent<HTMLInputElement>) => setShowParticipatedActor(event.target.checked)} />}
                label="Ajouter des acteurs participant déjà à l'événement"
              />
            )}
          {showParticipatedActor && (
            <>
              <AutocompleteField
                name="actors"
                label="Acteurs participants"
                options={(allActorsData?.actors ?? []).map((s) => ({ id: s.id, label: s.name }))}
                multiple
              
              />
            </>
          )}
        <FormControlLabel
            control={
              <Checkbox
              checked={hasParentEvent}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                if (hasParentEvent) {
                  setValue('parentEvent', null);
                }
                setHasParentEvent(event.target.checked);
              }}
              />
            }
            label="est affilié à un autre événement existant"
            />
          {hasParentEvent && (
            <AutocompleteField
                name="parentEvent"
                label="Evénement parent"
                options={(allEventsData?.events ?? []).map((s) => ({ id: s.id, label: s.label }))}
              />
          )}

          {proposeEvent !== undefined && (
            <>
              <br />
              <br />
              <AutocompleteField
                name="referencingActor"
                label="Vous ajoutez l'événement en tant que"
                options={(data?.actors || []).map((u) => ({
                  id: u.id,
                  label: `${u.name}`,
                }))}
              />
              {data && (
                <>
                  Un email sera envoyé à pour le prévenir que vous avez ajouté une action à sa place.
                </>
              )}
            </>
          )}
          <Tooltip title={errorMessages.length > 0 ? errorMessages.map((e) => <div>{e}</div>) : ''} arrow>
            <Box sx={{ margin: 'auto' }}>
              <LoadingButton disabled={errorMessages.length > 0} loading={loading} type="submit" variant="contained">
                {submitLabel}
              </LoadingButton>
            </Box>
          </Tooltip>

          {additionalButton}
        </Stack>
      </Box>
    </FormProvider>
  );
};

export default EventForm;

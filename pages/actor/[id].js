// GraphQL query to fetch the actor's name by URL
const GET_ACTOR_SSR = `
  query actor($id: String!) {
    actor(id: $id) {
      id
      url
    }
  }
`;

export async function getServerSideProps(ctxt) {
  // Fetch data from the API

  const res = await fetch(process.env.NEXT_PUBLIC_API_URI, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      operationName: 'actor',
      variables: {
        id: ctxt.params.id, // ctxt.params.id is being used as the actor's URL
      },
      query: GET_ACTOR_SSR,
    }),
  });

  const initialData = await res.json();
  // Check for errors or missing actor
  if (initialData.errors || !initialData.data?.actor) {
    console.error(
      `Error fetching actor with ID ${ctxt.params.id}. Error message: ${
        initialData.errors ? initialData.errors[0].message : 'Actor not found'
      }`,
    );
    // You might want to handle the error by returning a 404 or another appropriate response
    return {
      notFound: true, // Return a 404 if actor is not found
    };
  }
  // Redirect to the new route with the actor's name
  return {
    redirect: {
      destination: `/acteur/${initialData.data.actor.url}`, // Redirect to the new URL with actor's name
      permanent: true, // Use permanent (301) redirect
    },
  };
}

// The page component itself won't render anything since it's used for redirecting
export default function ActorPage() {
  return null; // Nothing is rendered
}

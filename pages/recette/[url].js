import React, {
  useEffect, useState, useRef, useMemo,
} from 'react';
import AppLayout from 'containers/layouts/AppLayout';
import { Container, Grid, Typography, useTheme } from '@mui/material';
import { withApollo } from 'hoc/withApollo.jsx';
import Head from 'next/head';
import Box from '@mui/material/Box';
import makeStyles from '@mui/styles/makeStyles';
import Parser from 'html-react-parser';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import InfoIcon from '@mui/icons-material/Info';
import useMediaQuery from '@mui/material/useMediaQuery';
import Tooltip from '@mui/material/Tooltip';
import { styled } from '@mui/material/styles';
import RichTextContent from 'components/RichTextContent';
import recipeCategories from 'src/recipeCategories';
import Image from 'next/image';
import {
  getImageUrl
} from '../../utils/utils';
import Link from 'components/Link';
import Fab from '@mui/material/Fab';
import EditIcon from '@mui/icons-material/Edit';
import { useSessionState } from '../../context/session/session';
import Modal from '@mui/material/Modal';
import IconButton from '@mui/material/IconButton';	
import CloseIcon from '@mui/icons-material/Close';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
const GET_RECEIPE = `
  query recipe($url: String) {
    recipe(url: $url) {
      id
      category
      label
      content
      time
      url
      nbPerson
      pictures {
        id
        originalPicturePath
        label
        main
      }
      user {
        id
        surname
        lastname
      }
      ingredients {
        id
        name
        quantity
        unit
        description
        IngredientBaseAlim {
          produit
          poids
          energie
          proteines
          lipides
          glucides
          empreinteCarbone
          agriculture
          transformation
          emballage
          transport
          distribution
          poidsParUnite
          densite
          poidsParCuillereASoupe
          poidsParCuillereACafe
        }
      }
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  img: {
    padding: '1em',
    maxHeight: '500px',
    width: 'inherit!important',
  },
  imgModal: {
    padding: '1em',
    maxHeight: '40em',
    width: 'inherit!important',
  },
  cardInfo: {
    // backgroundImage: "url('/icons/planet.svg')",
   //  backgroundImage:`url('./fond.png')`,
   backgroundColor: 'white',
   backgroundPosition: 'right',
   backgroundRepeat: 'no-repeat',
   backgroundOpacity: ' 0.5',
   borderRadius: '0.5em',
   justify: 'center',
   marginTop: ({ hasBannerUrl }) => (hasBannerUrl ? -53 : 20),
   alignItems: 'center',
   boxShadow: '0px 0px 38px -14px rgba(0, 0, 0, 0.46)',
   [theme.breakpoints.up('sm')]: {
     width: '80%',
     padding: '5em',
   },
   [theme.breakpoints.down('md')]: {
     paddingTop: 16,
   },
 },
  containerDescription: {
      justifyContent: 'space-evenly',
  },
  title: {
    marginTop: '3em',
    marginBottom: '2em'
  },
  bannerUrl: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    position: 'relative!important',
    maxHeight: '250px',
  },
  fab: {
    position: 'fixed',
    bottom: '40px',
    right: '40px',
    zIndex: '1400',
    backgroundColor: '#2C367E',
    color: 'white',
    '&:hover': {
      color: '#2C367E',
    },
  },
  align: {
    'text-align': 'center',
  },
  description: {
    wordBreak: 'break-word',
    textAlign: 'justify',
    fontSize: '1rem !important',
    [theme.breakpoints.up('sm')]: {
      paddingLeft: '2em',
    },
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  cardTitle: {
    color: theme.typography.h5.color,
    fontFamily: theme.typography.h5.fontFamily,
    textTransform: 'uppercase',
    textAlign: 'center',
    fontWeight: '400',
    fontSize: '2.5rem !important',
    [theme.breakpoints.down('md')]: {
      fontSize: '1.5rem !important',
    },
    border: {
      width: '3em',
      borderColor: '#2C367E',
      borderBottom: 'solid',
      borderBottomColor: '#2C367E',
      color: '#2C367E',
      height: '1em',
    },
  },
  ingredientInfoGrid: {
    position: 'relative',
    textAlign: 'center',
    backgroundColor: '#ededf5',
    borderRadius: 5,
    '& > *:first-child': {
      border: 'none',
    },
  },
}));
function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: 'block' }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: 'block' }}
      onClick={onClick}
    />
  );
}

const RecipeByUrl = ({ initialData }) => {
  const { data } = initialData;

  const user = useSessionState();
  const bannerUrl = useMemo(() => {
    return (data?.recipe?.pictures || []).filter((picture) => picture.main).length >= 1
      ? data.recipe.pictures.filter((picture) => picture.main)[0].originalPicturePath
      : null;
  }, [data]);
  const [openModalSlider, setOpenModalSlider] = useState(false);
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '80%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('md'));
  const Banner = styled(Container)(({ theme,url }) => ({
    marginTop: theme.spacing(2),
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    height: '24em',
    color: 'white',
    'text-align': 'center',
    padding: '3em',
    backgroundImage: url,
  }));

  const settingsSliderModal = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 2000,
    //  pauseOnHover: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  const maxSlideToShowImaget = !matches ? 3 : 1;
  const settingsSliderImage = {
    infinite: true,
    slidesToShow:
      data && data.recipe.pictures && data.recipe.pictures.length >= maxSlideToShowImaget
        ? maxSlideToShowImaget
        : data && data.recipe.pictures && data.recipe.pictures.length,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 2000,
    //  pauseOnHover: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  const showCarbonFootPrint = (carbonFootprint) => {
    if (carbonFootprint ==0) {
      carbonFootprint ="Non calculé"
    }
      else  if (carbonFootprint <0.001) {
          carbonFootprint = (carbonFootprint * 1000).toFixed(2) + ' g';
          
        } else {
          carbonFootprint = (parseFloat(carbonFootprint)).toFixed(2);
        }
    return carbonFootprint;
  };

      const calculateEqui = (quantity, baseIngredientAlim, value, unit) => {
       
        if(baseIngredientAlim && baseIngredientAlim!==null){
          
        let coefficient = 1;
        if(unit === 'g') {
          coefficient = 0.001;
        }
        if(unit === 'mg') {
          coefficient  =0.0001;
        }
        
        if(unit === 'unity') {
          coefficient = baseIngredientAlim.poidsParUnite;
        }
        if(unit === 'kg') {
          coefficient = 1;
        }
        if(unit === 'L') {
          coefficient = 1*baseIngredientAlim.densite;
        }
        if(unit === 'cl') {
          coefficient = 0.01*baseIngredientAlim.densite;
        }
        if(unit === 'ml') {
          coefficient = 0.001*baseIngredientAlim.densite;
        }
        if(unit === 'cuillère à soupe') {
          coefficient =baseIngredientAlim.poidsParCuillereASoupe;
        }
        if(unit === 'cuillère à café') {
          coefficient = baseIngredientAlim.poidsParCuillereACafe;
        }
        
        return quantity * value * coefficient;
      }else{
        return 0;
      }
      };
      function formatNumber(value) {
        return Number.isInteger(value) ? value : parseFloat(value.toFixed(2));
      }
      const textExplantationCarbonFootprintCalculation = (ingredientBaseAlim) => {
        const agriculture = (ingredientBaseAlim?.agriculture * 100).toFixed(2);
        const transformation = (ingredientBaseAlim?.transformation * 100).toFixed(2);
        const emballage = (ingredientBaseAlim?.emballage * 100).toFixed(2);
        const transport = (ingredientBaseAlim?.transport * 100).toFixed(2);
        const distribution = (ingredientBaseAlim?.distribution * 100).toFixed(2);

        const title = `Réparation du calcul de l'empreinte carbone:
          Agriculture: ${agriculture}%
          Transformation: ${transformation}%
          Emballage: ${emballage}%
          Transport: ${transport}%
          Distribution: ${distribution}%`;

        return title;
      };
      

      const stylesProps = useMemo(() => ({
        topImageSize: '250px',
        headerDisplay: 'static',
        hasBannerUrl: bannerUrl !== null,
      }), []);
      const styles = useStyles(stylesProps);
      const columnCarbonFootPrintSum = data.recipe.ingredients.reduce((sum, ingredient) => {
        let carbonFootprint =0;
        if( ingredient.IngredientBaseAlim &&  ingredient.IngredientBaseAlim!==null ){
          carbonFootprint =  calculateEqui(
            ingredient.quantity,
            ingredient.IngredientBaseAlim,ingredient.IngredientBaseAlim.empreinteCarbone,
            ingredient.unit
          );
        }
        return sum + parseFloat(carbonFootprint);
      }, 0);

      const columnLipideSum = data.recipe.ingredients.reduce((sum, ingredient) => {
        let lipide =0;
        if( ingredient.IngredientBaseAlim &&  ingredient.IngredientBaseAlim!==null ){
          lipide =  calculateEqui(
            ingredient.quantity,
            ingredient.IngredientBaseAlim,ingredient.IngredientBaseAlim.lipides,
            ingredient.unit
          );
        } 
        return sum + parseFloat(lipide.toFixed(0));
      }, 0);
      const columnGlucideSum = data.recipe.ingredients.reduce((sum, ingredient) => {
        let glucide = 0;
        if (ingredient.IngredientBaseAlim && ingredient.IngredientBaseAlim !== null) {
          glucide = calculateEqui(
        ingredient.quantity,
        ingredient.IngredientBaseAlim, ingredient.IngredientBaseAlim.glucides,
        ingredient.unit
          );
        }
        return sum + parseFloat(glucide);
      }, 0);
      const columnProteineSum = data.recipe.ingredients.reduce((sum, ingredient) => {
        let proteine =0;
        if( ingredient.IngredientBaseAlim &&  ingredient.IngredientBaseAlim!==null ){
          proteine =  calculateEqui(
            ingredient.quantity,
            ingredient.IngredientBaseAlim,ingredient.IngredientBaseAlim.proteines,
            ingredient.unit
          );
        }
        return sum + parseFloat(proteine);
      }, 0);
      const columnCaloriesSum = data.recipe.ingredients.reduce((sum, ingredient) => {
        let calories =0;
        if( ingredient.IngredientBaseAlim &&  ingredient.IngredientBaseAlim!==null ){
          calories =  calculateEqui(
            ingredient.quantity,
            ingredient.IngredientBaseAlim,ingredient.IngredientBaseAlim.energie,
            ingredient.unit
          );
        }
        return sum + parseFloat(calories.toFixed(0));
      }, 0);

      const [showModal, setShowModal] = useState(false);

      const handleImageClick = () => {
        setShowModal(true);
      };

  
      return (
        <AppLayout>
          <Head>
            <title>
              {'Recette Végétarienne avec empreinte carbone : '}
              {data?.recipe.label}
            </title>
            <meta key="description" name="description" content={data && (`${data.recipe.label}`)} />
            {data?.recipe?.pictures?.length >= 1
              && data.recipe.pictures.filter((picture) => picture.main).length >= 1 && (
                <meta
                  key="og:image"
                  property="og:image"
                  content={
                    data.recipe.pictures.length >= 1
                      ? getImageUrl(
                        data.recipe.pictures.filter((picture) => picture.main)[0]
                          .originalPicturePath,
                      )
                      : ''
                  }
                />
            )}
            {data?.recipe?.pictures?.length >= 1
              && data.recipe.pictures.filter((picture) => picture.main).length >= 0 && (
              <meta
                property="og:image"
                content={getImageUrl(bannerUrl)}
              />
            )}
            <meta key="og:title" property="og:title" content={data && (`${data.recipe.label}`)} />
            <meta key="og:description" property="og:description" content={data && data.recipe.shortDescription} />
            <meta key="twitter:title" name="twitter:title" content={data && (`${data.recipe.label}`)} />
            <meta key="twitter:description" name="twitter:description" content={data && data.recipe.shortDescription} />
            <meta key="og:url" property="og:url" content={`${process.env.NEXT_PUBLIC_BASE_URL}/recette/${data.recipe.url}`} />
        <meta key="twitter:url" property="twitter:url" content={`${process.env.NEXT_PUBLIC_BASE_URL}/recette/${data.recipe.url}`} />
          </Head>
          <Box>
          {bannerUrl && (
              <Banner
              url={`url("${getImageUrl(bannerUrl)}")`}
             />
          )}
            <Container className={styles.cardInfo}>
             
            <Grid container className={styles.title}>
                    <div style={{ width: "100%" }}>
                    <Typography variant="h1" className={styles.cardTitle} align="center">
                      {data && data.recipe.label}
                    </Typography>
                  
                    <br/>
                  
                    <Typography variant="h2" align="center">
                      {data && recipeCategories[data.recipe.category]}
                    </Typography>
                    <br/>
                    </div>
                    <div className={styles.border} />
                    <br/>
              </Grid>
              { data && data.recipe.nbPerson && (
                <Grid container>
                  <br/>
                    Nombre de personne : {data.recipe.nbPerson}
                    <br/>
                    <br/>
                  </Grid>
              )}
              <Grid container className={styles.containerDescription}>
           
                <Grid item md={5} sm={10} className={[styles.align]}>
                  <Grid container className={[styles.ingredientInfoGrid]}>
                    <TableContainer>
                      <Table aria-label="Ingredient Table" size="small">
                        <TableHead>
                          <TableRow sx={{ backgroundColor: '#f4f4f4', fontWeight: 'bold' }}>
                            <TableCell>Produit</TableCell>
                            <TableCell align="right">Qte</TableCell>
                            <TableCell align="right">Unité</TableCell>
                            <TableCell align="right">Empreinte Carbone (kg CO2)</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          { data.recipe.ingredients.map((ingredient) => (
                            <TableRow key={ingredient.id}
                            sx={{
                              backgroundColor: ingredient.id % 2 === 0 ? '#fff' : '#f9f9f9',
                              '&:hover': { backgroundColor: '#e6f7ff' },
                            }}
                            >
                              <TableCell>{ingredient.IngredientBaseAlim && ingredient.IngredientBaseAlim.produit ? ingredient.IngredientBaseAlim.produit : ingredient.name}</TableCell>
                              <TableCell align="right">{ingredient.quantity}</TableCell>
                             
                                <TableCell align="right"> {ingredient.unit !== 'unity' && (<>{ingredient.unit}</>)}</TableCell>
                              
                              <TableCell align="right" style={{ fontSize: '0.775rem' }}>
                                {showCarbonFootPrint(calculateEqui(
                                  ingredient.quantity,
                                  ingredient.IngredientBaseAlim,
                                  ingredient.IngredientBaseAlim?.empreinteCarbone,
                                  ingredient.unit
                                ))}
                                {ingredient.IngredientBaseAlim != null && (
                                  <>
                                    {' '}
                                    <Tooltip title={textExplantationCarbonFootprintCalculation(ingredient.IngredientBaseAlim)}>
                                      <InfoIcon />
                                    </Tooltip>
                                  </>
                                )}
                              </TableCell>
                            </TableRow>
                          ))}
                           <TableRow style={{ backgroundColor: "#f0f8ff" }}>
                              <TableCell>Calories<br/>Lipides<br/>Glucides<br/>Protéines</TableCell>
                              <TableCell colSpan={2}><table style={{ width: "100%", textAlign: "left", borderCollapse: "collapse" }}>
                                <tbody>
                                  <tr>
                                    <td style={{ textAlign: "center" }}>{formatNumber(columnCaloriesSum)}</td>
                                    <td style={{ textAlign: "right" }}>kcal</td>
                                  </tr>
                                  <tr>
                                    <td style={{ textAlign: "center" }}>{formatNumber(columnLipideSum)}</td>
                                    <td style={{ textAlign: "right" }}>g</td>
                                  </tr>
                                  <tr>
                                    <td style={{ textAlign: "center" }}>{formatNumber(columnGlucideSum)}</td>
                                    <td style={{ textAlign: "right" }}>g</td>
                                  </tr>
                                  <tr>
                                    <td style={{ textAlign: "center" }}>{formatNumber(columnProteineSum)}</td>
                                    <td style={{ textAlign: "right" }}>g</td>
                                  </tr>
                                </tbody>
                              </table></TableCell>
                              <TableCell align="right" sx={{ fontSize: '1rem',fontWeight:'600', color: '#007bff' }}>
                            
                               Total :  {showCarbonFootPrint(formatNumber(columnCarbonFootPrintSum))} <br/> kg CO2
                    
                              </TableCell>
                            </TableRow>
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </Grid>
              
                </Grid>
                <Grid item md={7} sm={10} className={styles.description}>
                  {data &&<RichTextContent content={data.recipe.content} />}
                </Grid>
                </Grid>
              {data && data.recipe.pictures && data.recipe.pictures.length > 0 && (
              <div>
                <Typography variant="h5" className={styles.cardTitle}>
                  PHOTOS
                </Typography>
                <div className={styles.border} />
                <br />
              </div>
            )}
            <Slider {...settingsSliderImage} className={[styles.slider]}>
              {data
                && data.recipe.pictures
                && data.recipe.pictures
                  .sort((a, b) => (a.position > b.position ? 1 : -1))
                  .map((picture) => (
                    <img
                      src={getImageUrl(picture.originalPicturePath)}
                      className={[styles.img]}
                      onClick={() => setOpenModalSlider(true)}
                    />
                  ))}
            </Slider>
            <Modal
              open={openModalSlider}
              onClose={() => setOpenModalSlider(false)}
              aria-labelledby="parent-modal-title"
              aria-describedby="parent-modal-description"
            >
              <Box sx={style}>
                <IconButton
                  aria-label="Close"
                  className={styles.closeButton}
                  onClick={() => setOpenModalSlider(false)}
                  size="large">
                  <CloseIcon />
                </IconButton>
                <Slider {...settingsSliderModal} className={[styles.slider]}>
                  {data
                    && data.recipe.pictures
                    && data.recipe.pictures
                      .sort((a, b) => (a.position > b.position ? 1 : -1))
                      .map((picture) => (

                        <img
                          src={getImageUrl(picture.originalPicturePath)}
                          className={[styles.imgModal]}
                          onClick={() => setOpenModalSlider(true)}
                        />
                      ))}
                </Slider>
              </Box>
            </Modal>
                  <Grid container>
                  <br/>
                    Proposé par : {data.recipe.user?.surname} {data.recipe.user?.lastname}
                  </Grid>
            
             
            </Container>

            {
              (data && (user && data.recipe?.user?.id === user.id ) || (user && user.role === 'admin')) && (
                <Link href={`/admin/recipes/${ data.recipe.id}`}>
                  <Fab className={styles.fab} aria-label="edit">
                    <EditIcon />
                  </Fab>
                </Link>
              )
            }
          </Box>
        </AppLayout>

);
};


export default withApollo()(RecipeByUrl);

// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// revalidation is enabled and a new request comes in
export async function getServerSideProps(ctxt) {
  const res = await fetch(process.env.NEXT_PUBLIC_API_URI, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      operationName: 'recipe',
      variables: {
        url: ctxt.params.url,
      },
      query: GET_RECEIPE,
    }),
  });

  const initialData = await res.json();

  if (initialData.errors) {
    console.error(
      ` Error fetching recipe id ${ctxt.params.id
      } error message : ${initialData.errors[0].message
      }`,
    );
  } else {
    console.log('initialData', initialData.data.recipe);
  }

  return {
    props: {
      initialData,
    },
  };
}

import React, { useCallback, useEffect } from 'react';
import { Container, Typography } from '@mui/material';
import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';
import { SubmitHandler } from 'react-hook-form';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import { withApollo } from '../../../hoc/withApollo';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay';
import { formatPicture, uploadPictures } from '../../../components/fields/ImageUploadField';
import ActorForm, { ActorFields } from 'containers/forms/ActorForm';
import { useSessionDispatch, useSessionState } from 'context/session/session';
import { transformToEntriesWithInformation, transformEntries } from '../../../src/utils/formUtils';
const ADD_STAGE = gql`
  mutation createActor(
    $actorInfos: ActorInfos
    $userId: Int!
    $description: String
    $volunteerDescription: String
    $logoPictures: [InputPictureType]
    $mainPictures: [InputPictureType]
    $pictures: [InputPictureType]
    $openingHours: [InputOpeningHour]
  ) {
    createActor(
      actorInfos: $actorInfos
      userId: $userId
      description: $description
      volunteerDescription: $volunteerDescription
      pictures: $pictures
      mainPictures: $mainPictures
      logoPictures: $logoPictures
      openingHours: $openingHours
    ) {
      id
      name
      url
    }
  }
`;

const AddActor = () => {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const user = useSessionState();
  const [addActor, { data, loading, error }] = useMutation(ADD_STAGE);
  const { proposeNewActor } = router.query;
  useGraphQLErrorDisplay(error);
  useEffect(() => {
    if (data) {
      enqueueSnackbar('Page acteur créée avec succès.', {
        preventDuplicate: true,
      });
      router.push(`/acteur/${data.createActor.url}`);
    }
  }, [data]);

  const handleSubmit: SubmitHandler<ActorFields> = useCallback(async (formValues) => {
    const {
      name,
      address,
      email,
      phone,
      shortDescription,
      description,
      mainPicture,
      website,
      socialNetwork,
      activity,
      entries,
      entriesWithInformation,
      volunteerDescription,
      siren,
      enableOpenData,
      memberOf,
      referencingActor,
      removeReferencingActor,
      logoPicture,
      pictures,
      openingHours,
      referents
    } = formValues;

    if(logoPicture){
    await uploadPictures([ ...logoPicture,...mainPicture, ...pictures,]);
    }

    addActor({
      variables: {
        actorInfos: {
          name,
          email,
          phone,
          address: address.address,
          postCode: address.postcode,
          city: address.city,
          shortDescription,
          lat: address.lat,
          lng: address.lng,
          activity,
          website,
          socialNetwork,
          entries : transformEntries(entries),
          entriesWithInformation : transformToEntriesWithInformation(entriesWithInformation),
          siren,
          enableOpenData,
          memberOf,
          referencingActor,
          removeReferencingActor,
          referents
        },
        openingHours,
        volunteerDescription,
        userId: parseInt(user.id),
        description,
        mainPictures: mainPicture?.map((picture) => ({
          main: true,
          logo: false,
          ...formatPicture(picture),
        })),
        pictures: pictures?.map((picture) => ({
          main: false,
          logo: false,
          ...formatPicture(picture),
        })),
        logoPictures: logoPicture?.map((picture) => ({
          logo: true,
          main: false,
          ...formatPicture(picture),
        })),
      },
    });
  }, []);

  return (
    <AdminPageLayout authorizedRoles={['admin','user']}>
      <Container maxWidth="md">
        <Typography color="secondary" variant="h2" textAlign="center">
          {proposeNewActor ===null && (
            <>Ajouter une page acteur</>
          )}
          {proposeNewActor ==='true' && (
            <>Proposer un nouvel acteur</>
          )}
        </Typography>

        <ActorForm
          loading={loading || data?.createActor}
          submitLabel={proposeNewActor==='true'?"Ajouter cet acteur":"Créer la page acteur"}
          onSubmit={handleSubmit}
          proposeNewActor ={proposeNewActor==='true'}
          defaultValues={{   referents: [user.id] }}
        />
      </Container>
    </AdminPageLayout>
  );
};

export default withApollo()(AddActor);

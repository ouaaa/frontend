import React, { useCallback, useEffect, useState } from 'react';
import { Container, Typography } from '@mui/material';
import { useMutation, useQuery } from '@apollo/client';
import gql from 'graphql-tag';
import { SubmitHandler } from 'react-hook-form';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import { withApollo } from '../../../hoc/withApollo';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay';
import { formatPicture, uploadPictures } from '../../../components/fields/ImageUploadField';
import { useSessionState } from 'context/session/session';
import LoadingButton from '@mui/lab/LoadingButton';
import ActorForm, { ActorFields } from 'containers/forms/ActorForm';
import DeletionModal from 'components/modals/DeletionModal';
import { transformToEntriesWithInformation, transformEntries } from 'src/utils/formUtils';
import { amET } from '@mui/material/locale';

const GET_ACTOR = gql`
  query actor($actorId: String!) {
    actor(id: $actorId) {
      id
      name
      email
      phone
      address
      postCode
      city
      website
      socialNetwork
      siren
      hasVideoVouaaar
      enableOpenData
      description
      url
      lat
      lng
      activity
      shortDescription
      volunteerDescription
      pictures {
        id
        label
        originalPicturePath
        originalPictureFilename
        position
        logo
        main
      }
      entries {
        id
        label
        icon
        description
        actorEntries {
          linkDescription
          topSEO
          id
        }
        parentEntry {
          id
          code
          label
          collection {
            id
            code
            label
          }
          parentEntry {
          id
          code
          label
          collection {
            id
            code
            label
          }
        }
        }
        subEntries {
          id
          code
          label
          icon
          description
          actorEntries {
            linkDescription
            topSEO
            id
          }
          collection {
            id
            code
            label
          }
        }
        collection {
          id
          code
          label
        }
      }
      contact_id
      openingHours {
        id
        days {
          id
          day
          selected
          identifier
        }
        hours
        place
      }
      referents {
        id
        surname
        lastname
      }
      memberOf {
        id
        name
      }
      referencingActor {
        id
        name
      }
      amapContratLegumes
      amapAutresContrats
      fermeModeDistribution
    }
  }
`;


const EDIT_ACTOR = gql`
  mutation editActor(
    $actorInfos: ActorInfos
    $actorId: Int!
    $userId: Int!
    $description: String!
    $volunteerDescription: String
    $logoPictures: [InputPictureType]
    $mainPictures: [InputPictureType]
    $pictures: [InputPictureType]
    $openingHours: [InputOpeningHour]
  ) {
    editActor(
      actorInfos: $actorInfos
      actorId: $actorId
      userId: $userId
      mainPictures: $mainPictures
      logoPictures: $logoPictures
      pictures: $pictures
      description: $description
      volunteerDescription: $volunteerDescription
      openingHours: $openingHours
    ) {
      id
      name
      email
      phone
      address
      postCode
      city
      website
      socialNetwork
      siren
      url
      description
      lat
      lng
      activity
      shortDescription
      volunteerDescription
      pictures {
        id
        label
        originalPicturePath
        originalPictureFilename
        position
        logo
        main
      }
      entries {
        id
        label
        icon
        color
        description
        actorEntries {
          linkDescription
          topSEO
          id
        }
        parentEntry {
          id
          code
          label
          collection {
            id
            code
            label
          }
        }
        subEntries {
          id
          code
          label
          icon
          description
          actorEntries {
            linkDescription
            topSEO
            id
          }
        }
        collection {
          id
          code
          label
        }
      }
      contact_id
      openingHours {
        id
        days {
          id
          day
          selected
          identifier
        }
        hours
        place
      }
      referents {
        id
        surname
        lastname
      }
      memberOf {
        id
        name
      }
    }
  }
`;

const DELETE_ACTOR = gql`
  mutation deleteActor($actorId: Int!, $deleteEvent: Boolean) {
    deleteActor(actorId: $actorId, deleteEvent: $deleteEvent)
  }
`;

const EditActor = () => {
  const { enqueueSnackbar } = useSnackbar();
  const user = useSessionState();
  const router = useRouter();
  const { id } = router.query;
  const { addAsReferent } = router.query;
  const { data: actorData, error: getError } = useQuery(GET_ACTOR, {
    variables: { actorId: id },
    fetchPolicy: 'no-cache',
  });
  const [editActor, { data, loading, error }] = useMutation(EDIT_ACTOR);
  const [deleteActor, { loading: deleteLoading, data: deleteData, error: deleteError }] = useMutation(DELETE_ACTOR);

  const [deletionModalOpen, setDeletionModalOpen] = useState(false);

  useGraphQLErrorDisplay(error);

  // Redirect if non-existent or non-authorized actor
  useEffect(() => {
    if (actorData?.actor && user?.role === 'user' && !(actorData?.actor.referencingActor && addAsReferent !== undefined)) {
      if (!actorData.actor.referents.map((r) => r.id).includes(user.id)) {
        router.push('/');
      }
    } else if (getError) {
      router.push('/');
    }
  }, [actorData, getError]);

  useEffect(() => {
    if (data && data.editActor) {
      enqueueSnackbar('Page acteur mise à jour avec succès.', {
        preventDuplicate: true,
      });
      router.push(`/acteur/${data.editActor.url}`);
      
    }
  }, [data]);

  useEffect(() => {
    if (!deleteLoading && deleteData?.deleteActor) {
      enqueueSnackbar('Page acteur supprimé.', {
        preventDuplicate: true,
      });
      router.push(`/admin/actors`);
    } else if (deleteError) {
      enqueueSnackbar("La suppression de la page acteur a échoué.", {
        preventDuplicate: true,
      });
    }
  }, [deleteData, deleteError, deleteLoading]);

  const handleDeletion = useCallback(() => {
    deleteActor({
      variables: {
        actorId: parseInt(`${id}`, 10),
        deleteEvent: true,
      },
    });
    setDeletionModalOpen(false);
  }, []);

  const handleSubmit: SubmitHandler<ActorFields> = useCallback(async (formValues) => {
    const {
      name,
      address,
      email,
      phone,
      shortDescription,
      description,
      mainPicture,
      website,
      socialNetwork,
      activity,
      entries,
      entriesWithInformation,
      volunteerDescription,
      siren,
      enableOpenData,
      memberOf,
      referencingActor,
      removeReferencingActor,
      openingHours,
      logoPicture,
      pictures,
      referents,
      amapContratLegumes,
      amapAutresContrats,
      fermeModeDistribution,

    } = formValues;

    await uploadPictures([...mainPicture, ...pictures]);

  
    editActor({
      variables: {
        actorId: parseInt(id, 10),
        userId: parseInt(user.id),
        actorInfos: {
          name,
          email,
          phone,
          address: address.address,
          postCode: address.postcode,
          city: address.city,
          shortDescription,
          lat: address.lat,
          lng: address.lng,
          activity,
          website,
          socialNetwork,
          entries : transformEntries(entries),
          entriesWithInformation : transformToEntriesWithInformation(entriesWithInformation),
          volunteerDescription,
          siren,
          enableOpenData,
          memberOf,
          referencingActor,
          referents,
          removeReferencingActor: addAsReferent==='true',
          amapContratLegumes,
          amapAutresContrats,
          fermeModeDistribution,

        },
        openingHours,
        description,
        mainPictures: mainPicture.map((picture) => ({
          main: true,
          logo: false,
          ...formatPicture(picture),
        })),
        pictures: pictures.map((picture) => ({
          main: false,
          logo: false,
          ...formatPicture(picture),
        })),
        logoPictures: logoPicture.map((picture) => ({
          logo: true,
          main: false,
          ...formatPicture(picture),
        })),
      },
    });
  }, []);

  if (!actorData?.actor) {
    return null;
  }

  const { actor } = actorData;

  const startedAt = new Date(parseInt(actor.startedAt, 10));
  const endedAt = new Date(parseInt(actor.endedAt, 10));
  const transformActorEntries = (entries) => {
  const entriesList=  entries.reduce((acc, entry) => {
      const collectionId = entry.collection?.id ||entry.parentEntry?.collection?.id ||entry.parentEntry?.parentEntry?.collection?.id || 'Uncategorized';
      const collectionKey = `entries.${collectionId}`;
  
      if (!acc[collectionKey]) {
        acc[collectionKey] = [];
      }
  
      acc[collectionKey].push(entry.id);
      return acc;
    }, {});
    return entriesList;
  };


  function loadEntriesWithInformation(entries) {
    const result = {};

    if(!entries){
      return null;
    }
  
    if (Array.isArray(entries)) {
      entries.forEach(entry => {
      if (entry.actorEntries && entry.actorEntries.linkDescription) {
        const key = `entries_${entry.id}`;
        result[key] = entry.actorEntries.linkDescription;
      }
      });
    }else{
      return null;
    }
    return result;
}
  
  return (
    <AdminPageLayout authorizedRoles={['user', 'admin','proposeActorRole','acteurAdminRole']}>
      <Container maxWidth="md">
        <Typography color="secondary" variant="h2" textAlign="center">
        
          {addAsReferent ? "Devenir référent d'une page acteur" : "Editer une page acteur"}
        </Typography>
        <ActorForm
          showReferents
          referentsList={user?.role === 'user' ? actor.referents : undefined}
          defaultValues={{
            address: {
              address: actor.address || actor.city,
              postcode: actor.postCode,
              city: actor.city,
              lat: parseFloat(actor.lat),
              lng: parseFloat(actor.lng),
            },
            name: actor.name,
            email: actor.email,
            phone: actor.phone,
            activity: actor.activity,
            socialNetwork: actor.socialNetwork,
            siren: actor.siren,
            enableOpenData: actor.enableOpenData,
            hasVideoVouaaar: actor.hasVideoVouaaar,
            shortDescription: actor.shortDescription,
            description: actor.description,
            website: actor.website,
            volunteerDescription: actor.volunteerDescription,
            volunteerAction: actor.volunteerAction,
            volunteerForm: actor.volunteerForm,
            openingHours: actor.openingHours,
            entriesWithInformation : loadEntriesWithInformation(actor.entries),
            ...transformActorEntries(actor.entries),
            entriesList : actor.entries,
            amapContratLegumes: actor.amapContratLegumes,
            amapAutresContrats: actor.amapAutresContrats,
            fermeModeDistribution: actor.fermeModeDistribution,
            mainPicture: actor.pictures
              .filter((p) => p.main )
              .map((p) => ({
                id: p.id,
                src: p.originalPicturePath,
                deleted: false,
              })),
            pictures: actor.pictures
              .filter((p) => !p.main && !p.logo)
              .map((p) => ({
                id: p.id,
                src: p.originalPicturePath,
                deleted: false,
              })),
              logoPicture: actor.pictures
              .filter((p) => p.logo)
              .map((p) => ({
                id: p.id,
                src: p.originalPicturePath,
                deleted: false,
              })),
            referents: addAsReferent?[user.id]:actor.referents.map((r) => r.id),
          }}
          submitLabel={addAsReferent?"Devenir référent":"Mettre à jour l'acteur"}
          loading={loading || data?.editActor}
          onSubmit={handleSubmit}
          additionalButton={
            !addAsReferent ? (
              <LoadingButton
                loading={deleteLoading}
                color="error"
                type="submit"
                variant="outlined"
                sx={{ margin: 'auto' }}
                onClick={(e) => {
                  setDeletionModalOpen(true);
                  e.preventDefault();
                }}
              >
                Supprimer l'acteur
              </LoadingButton>
            ) : undefined
          }
        />
      </Container>
      <DeletionModal
        open={deletionModalOpen}
        onClose={() => setDeletionModalOpen(false)}
        onSubmit={() => handleDeletion()}
        title="Êtes-vous sûr(e) de vouloir supprimer cette page acteur ?"
        content="Une fois supprimée, cette page acteur ainsi que ses événements associées seront définitivement supprimés"
      />
    </AdminPageLayout>
  );
};

export default withApollo()(EditActor);

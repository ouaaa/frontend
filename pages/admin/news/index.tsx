import React, { useCallback, useEffect, useState } from 'react';
import gql from 'graphql-tag';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Link,
  Stack,
  Typography,
} from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { DataGrid, GridColDef, frFR } from '@mui/x-data-grid';
import Paper from '@mui/material/Paper';
import Edit from '@mui/icons-material/Edit';
import Delete from '@mui/icons-material/Delete';
import { useMutation, useQuery } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useSessionState } from '../../../context/session/session';
import { withApollo } from '../../../hoc/withApollo';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay';
import DeletionModal from '../../../components/modals/DeletionModal';

const GET_ARTICLES = gql`
  query articlesAdmin ($userId: String!){
    articlesAdmin(userId: $userId) {
      id
      label
      url
      createdAt
      updatedAt
    }
  }
`;

const DELETE_ARTICLE = gql`
  mutation deleteArticle($articleId: Int!) {
    deleteArticle(articleId: $articleId)
  }
`;

const ArticleAdminPage = () => {
  const { enqueueSnackbar } = useSnackbar();
  const user = useSessionState();
  const { data, error: getError, refetch } = useQuery(GET_ARTICLES, {
    variables: {
      userId: user && user.id,
    },
  });
  const [deleteArticle, { loading: deleteLoading, data: deleteData, error: deleteError }] = useMutation(DELETE_ARTICLE);

  const [deletionPendingArticleId, setDeletionPendingArticleId] = useState<null | string>(null);

  useGraphQLErrorDisplay(getError);

  useEffect(() => {
    if (!deleteLoading && deleteData?.deleteArticle) {
      refetch();
      enqueueSnackbar('Article supprimée.', {
        preventDuplicate: true,
      });
    } else if (deleteError) {
      enqueueSnackbar("La suppression de l'article a échoué.", {
        preventDuplicate: true,
      });
    }
  }, [deleteData, deleteError, deleteLoading]);

  const handleDeletion = useCallback(() => {
    if (deletionPendingArticleId) {
      deleteArticle({
        variables: {
          articleId: parseInt(deletionPendingArticleId, 10),
        },
      });
      setDeletionPendingArticleId(null);
    }
  }, [deletionPendingArticleId]);

  const formatDate = (timestamp: string) => {
    const date = new Date(parseInt(timestamp, 10));
    return new Intl.DateTimeFormat('fr-FR', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    }).format(date);
  };
  const rows = data?.articlesAdmin || [];

  const columns: GridColDef<typeof rows[number]>[] = [
    {
      field: 'label',
      headerName: 'Nom',
      minWidth: 600,
      flex: 1,
      renderCell: (params) => (
        <Link href={`/article/${params.row.url}`} >{params.value}</Link>
      ),
    },
    {
      field: 'createdAt',
      headerName: 'Date de création',
      minWidth: 150,
      flex: 1,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: 'updatedAt',
      headerName: 'Date de modification',
      minWidth: 150,
      flex: 1,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: 'actions',
      headerName: 'Actions',
      type: 'actions',
      renderCell: (params) => (
        <>
          <IconButton aria-label="edit" component={Link} href={`/admin/news/${params.row.id}`}>
            <Edit />
          </IconButton>
          <IconButton aria-label="delete" onClick={() => setDeletionPendingArticleId(params.row.id)}>
            <Delete />
          </IconButton>
        </>
      ),
    },
  ];


  return (
    <AdminPageLayout  authorizedRoles={['user', 'admin','proposeActorRole','acteurAdminRole']}>
      <Stack direction={{ xs: 'column', sm: 'row' }} justifyContent="space-between" alignItems="center" sx={{mb: { xs: 3, md: 1 }}}>
        <Typography color="secondary" variant="h2">
          Liste des articles
        </Typography>

        <Button component={Link} href="/admin/news/add" variant="contained" size="small">
          Ajouter un nouvel article
        </Button>
      </Stack>
      <DataGrid
        localeText={frFR.components.MuiDataGrid.defaultProps.localeText}
        columns={columns}
        rows={rows}
        initialState={{
          sorting: {
            sortModel: [{ field: 'startedAt', sort: 'desc' }],
          },
          pagination: {
            paginationModel: {
              pageSize: 10,
            },
          },
        }}
        pageSizeOptions={[10]}
        disableRowSelectionOnClick
        autoHeight
      />
      <Dialog
        open={deletionPendingArticleId !== null}
        onClose={() => setDeletionPendingArticleId(null)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Êtes-vous sûr(e) de vouloir supprimer cette article ?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Une fois supprimée, cette article sera définitivement supprimée. Elle ne sera plus visible sur notre
            plateforme, ni pour vous, ni pour les visiteurs.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setDeletionPendingArticleId(null)} color="primary" variant="contained">
            Annuler
          </Button>
          <Button onClick={() => handleDeletion()} variant="outlined" color="primary" autoFocus>
            Supprimer
          </Button>
        </DialogActions>
      </Dialog>
      <DeletionModal
        open={deletionPendingArticleId !== null}
        onClose={() => setDeletionPendingArticleId(null)}
        onSubmit={() => handleDeletion()}
        title="Êtes-vous sûr(e) de vouloir supprimer cette article ?"
        content="Une fois supprimée, cet article sera définitivement supprimée. Elle ne sera plus visible sur notre plateforme, ni pour vous, ni pour les visiteurs."
      />
    </AdminPageLayout>
  );
};

export default withApollo()(ArticleAdminPage);

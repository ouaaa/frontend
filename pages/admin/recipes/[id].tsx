import React, { useCallback, useEffect } from 'react';
import { Container, Typography } from '@mui/material';
import { useMutation, useQuery } from '@apollo/client';
import gql from 'graphql-tag';
import { SubmitHandler } from 'react-hook-form';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import { withApollo } from '../../../hoc/withApollo.jsx';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout.tsx';
import RecipeForm, { RecipeFields } from '../../../containers/forms/RecipeForm';
import { useSessionDispatch, useSessionState } from 'context/session/session';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay.js';
import { formatPicture, uploadPictures } from 'components/fields/ImageUploadField';

const GET_RECIPE = gql`
  query recipe($id: String!) {
    recipe(id: $id) {
      id
      label
      category
      content
      shortDescription
      nbPerson
      pictures {
        id
        label
        originalPicturePath
        originalPictureFilename
        position
        logo
        main
      }
      ingredients {
        id
        name
        unit
        quantity
        baseAlimIngredientId
      }
      actor{
        id
        name
      }
    }
  }
`;

const EDIT_RECIPE = gql`
  mutation editRecipe(
    $recipe: RecipeInput
    $recipeId: Int!
    $mainPictures: [InputPictureType]
    $pictures: [InputPictureType]
  ) {
    editRecipe(
      recipe: $recipe
      recipeId: $recipeId
      mainPictures: $mainPictures
      pictures: $pictures
    ) {
      id
      category
      label
      content
      url
      ingredients {
        id
        name
        unit
        quantity
        baseAlimIngredientId
      }
    }
  }
`;

const EditRecipe = () => {
  const router = useRouter();
  const { id } = router.query;
  const user = useSessionState();
  const { enqueueSnackbar } = useSnackbar();
  const [editRecipe, { data: updateData, error: updateError }] = useMutation(EDIT_RECIPE);

  const { error: getError, data: recipeData } = useQuery(GET_RECIPE, {
    variables: { id },
    fetchPolicy: 'no-cache',
  });

  useGraphQLErrorDisplay(getError);
  useGraphQLErrorDisplay(updateError);

  useEffect(() => {
    if (updateData) {
      enqueueSnackbar('Recette modifiée avec succès.', {
        preventDuplicate: true,
      });
      router.push(`/recette/${updateData.editRecipe.url}`);
    }
  }, [updateData]);

  const handleSubmit: SubmitHandler<RecipeFields> = useCallback(async (formValues) => {
    const { label, shortDescription, content, mainPicture, pictures, actor, ingredients, nbPerson,category } = formValues;

    await uploadPictures([...mainPicture, ...pictures]);

    editRecipe({
      variables: {
        recipe: {
          label,
          shortDescription,
          nbPerson: parseInt(nbPerson),
          category,
          content,
          ingredients: ingredients.map((ingredient) => ({
            name: ingredient.name,
            unit: ingredient.unit,
            id : parseInt(ingredient.id),
            baseAlimIngredientId: (ingredient.baseAlimIngredientId && ingredient.baseAlimIngredientId!=="null" )?parseInt(ingredient.baseAlimIngredientId):null,
            quantity: parseInt(ingredient.quantity),
          })),
        },
        recipeId: parseInt(id, 10),
        actorId: parseInt(actor, 10),
        mainPictures: mainPicture.map((picture) => ({
          main: true,
          ...formatPicture(picture),
        })),
        pictures: pictures.map((picture) => ({
          main: false,
          ...formatPicture(picture),
        })),
      },
    });
  }, []);

  return (
    <AdminPageLayout authorizedRoles={['admin','user']}>
      <Container maxWidth="md">
        <Typography color="secondary" variant="h2" textAlign="center">
          Editer une recette
        </Typography>

        {recipeData && (
          <RecipeForm
            disabledFields={['actor']}
            defaultValues={{
              label: recipeData.recipe.label,
              shortDescription: recipeData.recipe.shortDescription,
              content: recipeData.recipe.content,
              category: recipeData.recipe.category,
              actor: recipeData.recipe.actor?.id,
              ingredients: recipeData.recipe.ingredients,
              nbPerson: recipeData.recipe.nbPerson,
              mainPicture: recipeData.recipe.pictures
                .filter((p) => p.main)
                .map((p) => ({
                  id: p.id,
                  src: p.originalPicturePath,
                  deleted: false,
                })),
              pictures: recipeData.recipe.pictures
                .filter((p) => !p.main)
                .map((p) => ({
                  id: p.id,
                  src: p.originalPicturePath,
                  deleted: false,
                })),
            }}
            submitLabel="Mettre à jour la recette"
            onSubmit={handleSubmit}
          />
        )}
      </Container>
    </AdminPageLayout>
  );
};

export default withApollo()(EditRecipe);

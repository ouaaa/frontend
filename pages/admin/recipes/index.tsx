import React, { useCallback, useEffect, useState } from 'react';
import gql from 'graphql-tag';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Link,
  Stack,
  Typography,
} from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Edit from '@mui/icons-material/Edit';
import Delete from '@mui/icons-material/Delete';
import { useMutation, useQuery } from '@apollo/client';
import { useSnackbar } from 'notistack';
import { useSessionState } from '../../../context/session/session';
import { withApollo } from '../../../hoc/withApollo';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay';

const GET_ARTICLES = gql`
  query recipesAdmin ($userId: String!){
    recipesAdmin(userId: $userId) {
      id
      label
      url
      createdAt
      updatedAt
    }
  }
`;

const DELETE_ARTICLE = gql`
  mutation deleteRecipe($recipeId: Int!) {
    deleteRecipe(recipeId: $recipeId)
  }
`;

const RecipeAdminPage = () => {
  const { enqueueSnackbar } = useSnackbar();
  const user = useSessionState();
  const { data, error: getError, refetch } = useQuery(GET_ARTICLES, {
    variables: {
      userId: user && user.id,
    },
  });
  const [deleteRecipe, { loading: deleteLoading, data: deleteData, error: deleteError }] = useMutation(DELETE_ARTICLE);

  const [deletionPendingRecipeId, setDeletionPendingRecipeId] = useState<null | string>(null);

  useGraphQLErrorDisplay(getError);

  useEffect(() => {
    if (!deleteLoading && deleteData?.deleteRecipe) {
      refetch();
      enqueueSnackbar('Recette supprimée.', {
        preventDuplicate: true,
      });
    } else if (deleteError) {
      enqueueSnackbar("La suppression de la recette.", {
        preventDuplicate: true,
      });
    }
  }, [deleteData, deleteError, deleteLoading]);

  const handleDeletion = useCallback(() => {
    if (deletionPendingRecipeId) {
      deleteRecipe({
        variables: {
          recipeId: parseInt(deletionPendingRecipeId, 10),
        },
      });
      setDeletionPendingRecipeId(null);
    }
  }, [deletionPendingRecipeId]);

  const formatDate = (timestamp: string) => {
    const date = new Date(parseInt(timestamp, 10));
    return new Intl.DateTimeFormat('fr-FR', {
      year: 'numeric',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    }).format(date);
  };

  return (
    <AdminPageLayout  authorizedRoles={['user', 'admin','proposeActorRole','acteurAdminRole']}>
      <Stack direction={{ xs: 'column', sm: 'row' }} justifyContent="space-between" alignItems="center" sx={{mb: { xs: 3, md: 1 }}}>
        <Typography color="secondary" variant="h2">
          Liste des recettes
        </Typography>

        <Button component={Link} href="/admin/recipes/add" variant="contained" size="small">
          Ajouter une nouvelle recette
        </Button>
      </Stack>

      <TableContainer component={Paper} sx={{ mb: 3 }}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Nom</TableCell>
              <TableCell align="center" sx={{ width: '20%' }}>
                Date de création
              </TableCell>
              <TableCell align="center" sx={{ width: '20%' }}>
                Date de modification
              </TableCell>
              <TableCell align="center" sx={{ width: '10%' }}>
                Actions
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data &&
              data.recipesAdmin.map((recipe) => (
                <TableRow key={recipe.id} hover>
                  <TableCell component="th" scope="row">
                    <Link href={`/recette/${recipe.url}`}>{recipe.label}</Link>
                  </TableCell>
                  <TableCell align="center">{formatDate(recipe.createdAt)}</TableCell>
                  <TableCell align="center">{formatDate(recipe.updatedAt)}</TableCell>
                  <TableCell align="center">
                    <IconButton aria-label="edit" component={Link} href={`/admin/recipes/${recipe.id}`}>
                      <Edit />
                    </IconButton>
                    <IconButton aria-label="delete" onClick={() => setDeletionPendingRecipeId(recipe.id)}>
                      <Delete />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Dialog
        open={deletionPendingRecipeId !== null}
        onClose={() => setDeletionPendingRecipeId(null)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Êtes-vous sûr(e) de vouloir supprimer cette recipe ?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Une fois supprimée, cette recipe sera définitivement supprimée. Elle ne sera plus visible sur notre
            plateforme, ni pour vous, ni pour les visiteurs.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setDeletionPendingRecipeId(null)} color="primary" variant="contained">
            Annuler
          </Button>
          <Button onClick={() => handleDeletion()} variant="outlined" color="primary" autoFocus>
            Supprimer
          </Button>
        </DialogActions>
      </Dialog>
    </AdminPageLayout>
  );
};

export default withApollo()(RecipeAdminPage);

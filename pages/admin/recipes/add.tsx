import React, { useCallback, useEffect } from 'react';
import { Container, Typography } from '@mui/material';
import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';
import { SubmitHandler } from 'react-hook-form';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import { withApollo } from '../../../hoc/withApollo';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout';
import RecipeForm, { RecipeFields } from '../../../containers/forms/RecipeForm';
import { useSessionDispatch, useSessionState } from 'context/session/session';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay';
import { formatPicture, uploadPictures } from '../../../components/fields/ImageUploadField';

const ADD_ARTICLE = gql`
  mutation createRecipe(
    $recipe: RecipeInput
    $actorId: ID
    $userId: ID
    $mainPictures: [InputPictureType]
    $pictures: [InputPictureType]
  ) {
    createRecipe(recipe: $recipe, mainPictures: $mainPictures, pictures: $pictures, actorId: $actorId, userId: $userId) {
      id
      category
      label
      url
      content
      ingredients {
        id
        name
        unit
        quantity
        baseAlimIngredientId
      }
    }
  }
`;

const AddRecipe = () => {
  const router = useRouter();
  const { actorId} = router.query;
  const user = useSessionState();
  const { enqueueSnackbar } = useSnackbar();
  const [createRecipe, { data, error }] = useMutation(ADD_ARTICLE);

  useGraphQLErrorDisplay(error);
  useEffect(() => {
    if (data) {
      enqueueSnackbar('Recette créée avec succès.', {
        preventDuplicate: true,
      });
      router.push(`/recette/${data.createRecipe.url}`);
    }
  }, [data]);

  const handleSubmit: SubmitHandler<RecipeFields> = useCallback(async (formValues) => {
    const { label, shortDescription, content, mainPicture, pictures,  actor ,ingredients,category} = formValues;

    await uploadPictures([...mainPicture, ...pictures]);

    createRecipe({
      variables: {
        recipe: {
          label,
          shortDescription,
          content,
          category,
          ingredients: ingredients.map((ingredient) => ({
            ...ingredient,
            baseAlimIngredientId: (ingredient.baseAlimIngredientId && ingredient.baseAlimIngredientId!=="null" )?parseInt(ingredient.baseAlimIngredientId):null,
            quantity: parseInt(ingredient.quantity),
          })),
        },
        actorId: parseInt(actor, 10),
        userId : parseInt(user.id),
        mainPictures: mainPicture.map((picture) => ({
          main: true,
          ...formatPicture(picture),
        })),
        pictures: pictures.map((picture) => ({
          main: false,
          ...formatPicture(picture),
        })),
      },
    });
  }, []);

  return (
    <AdminPageLayout authorizedRoles={['admin','user']}>
      <Container maxWidth="md">
        <Typography color="secondary" variant="h2" textAlign="center">
          Ajouter une recette
        </Typography>

        <RecipeForm submitLabel="Créer la recette" onSubmit={handleSubmit} 
        defaultValues={{
          actor: actorId ? `${actorId}` : undefined,
          ingredients: [],
        }}
        />
      </Container>
    </AdminPageLayout>
  );
};

export default withApollo()(AddRecipe);

import React, { useCallback, useEffect, useState } from 'react';
import { Container, Typography } from '@mui/material';
import { useMutation, useQuery } from '@apollo/client';
import gql from 'graphql-tag';
import { SubmitHandler } from 'react-hook-form';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import { withApollo } from '../../../hoc/withApollo';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay';
import { formatPicture, uploadPictures } from '../../../components/fields/ImageUploadField';
import EventForm, { EventFields } from 'containers/forms/EventForm';
import { useSessionState } from 'context/session/session';
import LoadingButton from '@mui/lab/LoadingButton';
import DeletionModal from 'components/modals/DeletionModal';
import { transformToEntriesWithInformation, transformEntries } from 'src/utils/formUtils';
import { da } from 'date-fns/locale';
const GET_EVENT = gql`
  query getEvent($eventId: String) {
    event(id: $eventId) {
      label
      shortDescription
      startedAt
      endedAt
      lat
      lng
      address
      postCode
      city
      description
      dateRule
      url
      entries {
        id
        label
        icon
        description
        actorEntries {
          linkDescription
          topSEO
          id
        }
        parentEntry {
          id
          code
          label
          collection {
            id
            code
            label
          }
        }
        subEntries {
          id
          code
          label
          icon
          description
          actorEntries {
            linkDescription
            topSEO
            id
          }
          collection {
            id
            code
            label
          }
        }
        collection {
            id
            code
            label
          }
      }
      participateButton
      registerLink
      limitPlace
      practicalInfo
      facebookUrl
      pictures {
        id
        originalPicturePath
        main
        logo
      }
      actors {
        id
        name
        referents {
          id
        }
      }
      parentEvent {
          id
          label
          startedAt
          endedAt
        }
    }
  }
`;

const EDIT_EVENT = gql`
  mutation editEvent(
    $eventInfos: EventInfos
    $eventId: Int!
    $description: String!
    $practicalInfo: String
    $logoPictures: [InputPictureType]
    $mainPictures: [InputPictureType]
    $pictures: [InputPictureType]
  ) {
    editEvent(
      eventInfos: $eventInfos
      eventId: $eventId
      description: $description
      practicalInfo: $practicalInfo
      pictures: $pictures
      logoPictures: $logoPictures
      mainPictures: $mainPictures
    ) {
      id
      url
    }
  }
`;

const DELETE_EVENT = gql`
  mutation deleteEvent($eventId: Int!) {
    deleteEvent(eventId: $eventId)
  }
`;
const transformEventEntries = (entries) => {
  return entries.reduce((acc, entry) => {
    const collectionId = entry.collection?.id ||entry.parentEntry?.collection?.id || 'Uncategorized';
    const collectionKey = `entries.${collectionId}`;

    if (!acc[collectionKey]) {
      acc[collectionKey] = [];
    }

    acc[collectionKey].push(entry.id);
    return acc;
  }, {});
};

const EditEvent = () => {
  const { enqueueSnackbar } = useSnackbar();
  const user = useSessionState();
  const router = useRouter();
  const { id } = router.query;

  const { data: eventData, error: getError } = useQuery(GET_EVENT, {
    variables: { eventId: id },
    fetchPolicy: 'no-cache',
  });
  const [editEvent, { data, loading, error }] = useMutation(EDIT_EVENT);
  const [deleteEvent, { loading: deleteLoading, data: deleteData, error: deleteError }] = useMutation(DELETE_EVENT);

  const [deletionModalOpen, setDeletionModalOpen] = useState(false);

  useGraphQLErrorDisplay(error);

  // Redirect if non-existent or non-authorized actor
  useEffect(() => {
    if (eventData?.event && user.role === 'user') {
      if (!eventData.event.actors[0].referents.map((r) => r.id).includes(user.id)) {
        router.push('/');
      }
    } else if (getError) {
      router.push('/');
    }
  }, [eventData, getError]);

  useEffect(() => {
    if (data && data.editEvent && eventData?.event) {
      enqueueSnackbar('Evénement modifié avec succès.', {
        preventDuplicate: true,
      });
      router.push(`/evenement/${data.editEvent.url}`);
    }
  }, [data, eventData]);

  useEffect(() => {
    if (!deleteLoading && deleteData?.deleteEvent) {
      enqueueSnackbar('Evénement supprimé.', {
        preventDuplicate: true,
      });
      router.push(`/admin/events`);
    } else if (deleteError) {
      enqueueSnackbar("La suppression de l'événément a échoué.", {
        preventDuplicate: true,
      });
    }
  }, [deleteData, deleteError, deleteLoading]);

  const handleDeletion = useCallback(() => {
    deleteEvent({
      variables: {
        eventId: parseInt(`${id}`, 10),
      },
    });
    setDeletionModalOpen(false);
  }, []);

  const handleSubmit: SubmitHandler<EventFields> = useCallback(async (formValues) => {
    const { label, address, shortDescription, description, facebookUrl, practicalInfo, startedAt, endedAt,participateButton,entries,emailInvitationText,actors,inviteActors,parentEvent,dateRule,registerLink,limitPlace } =
      formValues;
    await uploadPictures([...formValues.logoPicture, ...formValues.mainPicture, ...formValues.pictures]);
    editEvent({
      variables: {
        eventInfos: {
          label,
          shortDescription,
          startedAt,
          endedAt,
          lat: address.lat,
          lng: address.lng,
          address: address.address,
          postCode: address.postcode,
          city: address.city,
          facebookUrl,
          participateButton,
          entries : transformEntries(entries),
          actors,
          emailInvitationText,
          inviteActors,
          parentEventId: parentEvent,
          dateRule,
          registerLink,
          limitPlace : limitPlace ? parseInt(limitPlace, 10) : null,
        },
        eventId: parseInt(id, 10),
        description,
        practicalInfo,
        logoPictures: formValues.logoPicture.map((picture) => ({
          logo: true,
          main: false,
          ...formatPicture(picture),
        })),
        mainPictures: formValues.mainPicture.map((picture) => ({
          main: true,
          ...formatPicture(picture),
        })),
        pictures: formValues.pictures.map((picture) => ({
          main: false,
          ...formatPicture(picture),
        })),
      },
    });
  }, []);

  const transformActorEntries = (entries) => {
    const entriesList=  entries.reduce((acc, entry) => {
        const collectionId = entry.collection?.id ||entry.parentEntry?.collection?.id || 'Uncategorized';
        const collectionKey = `entries.${collectionId}`;
    
        if (!acc[collectionKey]) {
          acc[collectionKey] = [];
        }
    
        acc[collectionKey].push(entry.id);
        return acc;
      }, {});

      return entriesList;
    };
  function loadEntriesWithInformation(entries) {
    const result = {};

    if(!entries){
      return null;
    }
  
    if (Array.isArray(entries)) {
      entries.forEach(entry => {
      if (entry.actorEntries && entry.actorEntries.linkDescription) {
        const key = `entries_${entry.id}`;
        result[key] = entry.actorEntries.linkDescription;
      }
      });
    }else{
      return null;
    }

    return result;
}

  if (!eventData?.event) {
    return null;
  }

  const { event } = eventData;

  return (
    <AdminPageLayout authorizedRoles={['user', 'admin','proposeActorRole','acteurAdminRole']}>
      <Container maxWidth="md">
        <Typography color="secondary" variant="h2" textAlign="center">
          Editer un  événement
        </Typography>
        <EventForm
          disabledFields={['actor']}
          defaultValues={{
            actor: event.actors[0].id,
            actors : event.actors.map((r) => r.id),
            label: event.label,
            shortDescription: event.shortDescription,
            address: {
              address: event.address,
              postcode: event.postCode,
              city: event.city,
              lat: event.lat,
              lng: event.lng,
            },
            parentEvent: event.parentEvent?.id,
            startedAt: new Date(parseInt(event.startedAt, 10)).toISOString(),
            endedAt: new Date(parseInt(event.endedAt, 10)).toISOString(),
            description: event.description,
            facebookUrl: event.facebookUrl,
            practicalInfo: event.practicalInfo,
            participateButton : event.participateButton,
            registerLink: event.registerLink,
            limitPlace: event.limitPlace,
            dateRule : event.dateRule,
            ...transformActorEntries(event.entries),
            entriesList : event.entries,
            entriesWithInformation : loadEntriesWithInformation(event.entries),
            logoPicture: event.pictures
            
            .filter((p) => p.logo)
            .map((p) => ({
              id: p.id,
              src: p.originalPicturePath,
              deleted: false,
            })),
            mainPicture: event.pictures
              .filter((p) => p.main)
              .map((p) => ({
                id: p.id,
                src: p.originalPicturePath,
                deleted: false,
              })),
            pictures: event.pictures
              .filter((p) => !p.main && !p.logo)
              .map((p) => ({
                id: p.id,
                src: p.originalPicturePath,
                deleted: false,
              })),
          }}
          submitLabel="Mettre à jour l'activité"
          loading={loading || data?.editEvent}
          onSubmit={handleSubmit}
          additionalButton={
            <LoadingButton
              loading={deleteLoading}
              color="error"
              type="submit"
              variant="outlined"
              sx={{ margin: 'auto' }}
              onClick={(e) => {
                setDeletionModalOpen(true);
                e.preventDefault();
              }}
            >
              Supprimer l'activité
            </LoadingButton>
          }
        />
      </Container>
      <DeletionModal
        open={deletionModalOpen}
        onClose={() => setDeletionModalOpen(false)}
        onSubmit={() => handleDeletion()}
        title="Êtes-vous sûr(e) de vouloir supprimer cet événement ?"
        content="Une fois supprimée, cet événment sera définitivement supprimé."
      />
    </AdminPageLayout>
  );
};

export default withApollo()(EditEvent);

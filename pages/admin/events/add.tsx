import React, { useCallback, useEffect } from 'react';
import { Container, Typography } from '@mui/material';
import { useMutation } from '@apollo/client';
import gql from 'graphql-tag';
import { SubmitHandler } from 'react-hook-form';
import { useSnackbar } from 'notistack';
import { useRouter } from 'next/router';
import { withApollo } from '../../../hoc/withApollo';
import AdminPageLayout from '../../../containers/layouts/AdminPageLayout';
import useGraphQLErrorDisplay from '../../../hooks/useGraphQLErrorDisplay';
import { formatPicture, uploadPictures } from '../../../components/fields/ImageUploadField';
import EventForm, { EventFields } from 'containers/forms/EventForm';
import { useSessionState } from 'context/session/session';
import { transformToEntriesWithInformation, transformEntries } from '../../../src/utils/formUtils';
const ADD_EVENT = gql`
  mutation createEvent(
    $eventInfos: EventInfos
    $actorId: Int!
    $userId: Int!
    $description: String!
    $practicalInfo: String
    $logoPictures: [InputPictureType]
    $mainPictures: [InputPictureType]
    $pictures: [InputPictureType]
  ) {
    createEvent(
      eventInfos: $eventInfos
      actorId: $actorId
      userId: $userId
      description: $description
      practicalInfo: $practicalInfo
      logoPictures: $logoPictures
      pictures: $pictures
      mainPictures: $mainPictures
    ) {
      id
      url
      actors {
        id
        name
        url
      }
    }
  }
`;

const AddEvent = () => {
  const { enqueueSnackbar } = useSnackbar();
  const router = useRouter();
  const { actorId,proposeEvent} = router.query;
  const [addEvent, { data, loading, error }] = useMutation(ADD_EVENT);
  const user = useSessionState();
  useGraphQLErrorDisplay(error);

  useEffect(() => {
    if (data && data.createEvent) {
      enqueueSnackbar('Evénement créé avec succès.', {
        preventDuplicate: true,
      });
      router.push(`/evenement/${data.createEvent.url}`);
    }
  }, [data]);

  const handleSubmit: SubmitHandler<EventFields> = useCallback(async (formValues) => {
    const {
      label,
      address,
      shortDescription,
      description,
      practicalInfo,
      entries,
      startedAt,
      endedAt,
      facebookUrl,
      actor,
      referencingActor,
      participateButton,
      inviteActors,
      actors,
      emailInvitationText,
      parentEvent,
      dateRule,
      enabledRegistration,
      registerLink,
      limitPlace,

    } = formValues;
    await uploadPictures([...formValues.logoPicture,...formValues.mainPicture, ...formValues.pictures]);
    addEvent({
      variables: {
        eventInfos: {
          label,
          shortDescription,
          startedAt,
          endedAt,
          lat: address.lat,
          lng: address.lng,
          address: address.address,
          postCode: address.postcode,
          city: address.city,
          facebookUrl,
        
          referencingActor,
          entries : transformEntries(entries),
          inviteActors,
          actors,
          emailInvitationText,
          parentEventId: parentEvent,
          dateRule,
          participateButton,
          registerLink,
          limitPlace : limitPlace ? parseInt(limitPlace, 10) : null,

        },
        actorId: parseInt(actor, 10),
        userId: parseInt(user.id),
        description,
        practicalInfo,
        logoPictures: formValues.logoPicture.map((picture) => ({
          logo: true,
          main: false,
          ...formatPicture(picture),
        })),
        mainPictures: formValues.mainPicture.map((picture) => ({
          main: true,
          ...formatPicture(picture),
        })),
        pictures: formValues.pictures.map((picture) => ({
          main: false,
          ...formatPicture(picture),
        })),
      },
    });
  }, []);

  return (
    <AdminPageLayout authorizedRoles={['user', 'admin','proposeActorRole','acteurAdminRole']}>
      <Container maxWidth="md">
        <Typography color="secondary" variant="h2" textAlign="center">
          Ajouter un événement
        </Typography>
        <EventForm
          loading={loading || data?.createEvent}
          submitLabel="Ajouter l'événement"
          onSubmit={handleSubmit}
          defaultValues={{
            actor: actorId ? `${actorId}` : undefined,
            proposeEvent: proposeEvent ? `${proposeEvent}` : undefined,
          }}
        />
      </Container>
    </AdminPageLayout>
  );
};

export default withApollo()(AddEvent);

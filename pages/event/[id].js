// GraphQL query to fetch the event's name by URL
const GET_EVENT_SSR = `
  query event($id: String!) {
    event(id: $id) {
      id
      url
    }
  }
`;

export async function getServerSideProps(ctxt) {
  // Fetch data from the API

  const res = await fetch(process.env.NEXT_PUBLIC_API_URI, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      operationName: 'event',
      variables: {
        id: ctxt.params.id, // ctxt.params.id is being used as the event's URL
      },
      query: GET_EVENT_SSR,
    }),
  });

  const initialData = await res.json();
  // Check for errors or missing event
  if (initialData.errors || !initialData.data?.event) {
    console.error(
      `Error fetching event with ID ${ctxt.params.id}. Error message: ${
        initialData.errors ? initialData.errors[0].message : 'Actor not found'
      }`,
    );
    // You might want to handle the error by returning a 404 or another appropriate response
    return {
      notFound: true, // Return a 404 if event is not found
    };
  }
  // Redirect to the new route with the event's name
  return {
    redirect: {
      destination: `/evenement/${initialData.data.event.url}`, // Redirect to the new URL with event's name
      permanent: true, // Use permanent (301) redirect
    },
  };
}

// The page component itself won't render anything since it's used for redirecting
export default function EventPage() {
  return null; // Nothing is rendered
}

import AppLayout from 'containers/layouts/AppLayout';
import { Avatar, Button, Grid, Stack, TextField, Typography } from '@mui/material';
import { VpnKeyOutlined } from '@mui/icons-material';
import { useCallback, useEffect, useState } from 'react';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import { withApollo } from 'hoc/withApollo';
import useGraphQLErrorDisplay from 'hooks/useGraphQLErrorDisplay';
import UserPasswordForm from 'containers/forms/UserPasswordForm';
import { useSessionState } from 'context/session/session';
import { useRouter } from 'next/router';
import CodeField from 'components/fields/CodeField';

const SEND_PASSWORD_RESET_EMAIL = gql`
  mutation sendResetPasswordEmail($resetPasswordInfos: ResetPasswordInfos) {
    sendResetPasswordEmail(resetPasswordInfos: $resetPasswordInfos)
  }
`;

const VALIDATE_CODE = gql`
  mutation validateActionCode($validateActionCodeInfos: ValidateActionCodeInfos) {
    validateActionCode(validateActionCodeInfos: $validateActionCodeInfos)
  }
`;

const UPDATE_FORGOT_PASSWORD = gql`
  mutation updateForgotPassword($email: String!, $codeId: Int!, $code: Int!, $password: String!) {
    updateForgotPassword(email: $email, codeId: $codeId, code: $code, password: $password)
  }
`;

type FormStep = 'EMAIL_FORM' | 'CODE_FORM' | 'PASSWORD_FORM';

const ForgotPassword = () => {
  const user = useSessionState();
  const router = useRouter();

  const [formStep, setFormStep] = useState<FormStep>('EMAIL_FORM');
  const [email, setEmail] = useState('');
  const [codeId, setCodeId] = useState<number>();
  const [code, setCode] = useState<number>();

  const [sendResetPasswordEmail, { data: emailData, error: emailError }] = useMutation(SEND_PASSWORD_RESET_EMAIL);
  const [validateActionCode, { data: codeData, error: codeError }] = useMutation(VALIDATE_CODE);
  const [updateForgotPassword, { data: updateData, error: updateError }] = useMutation(UPDATE_FORGOT_PASSWORD);

  useGraphQLErrorDisplay(emailError);
  useGraphQLErrorDisplay(codeError);
  useGraphQLErrorDisplay(updateError);

  useEffect(() => {
    if (user) {
      router.push('/');
    }
  }, [user]);

  useEffect(() => {
    if (emailData?.sendResetPasswordEmail) {
      setCodeId(emailData?.sendResetPasswordEmail);
      setFormStep('CODE_FORM');
    }
  }, [emailData]);

  useEffect(() => {
    if (codeData?.validateActionCode) {
      setFormStep('PASSWORD_FORM');
    }
  }, [codeData]);

  useEffect(() => {
    if (codeError) {
      setFormStep('EMAIL_FORM');
    }
  }, [codeError]);

  useEffect(() => {
    if (updateData && updateData.updateForgotPassword) {
      router.push('/signin?from=forgotPassword');
    }
  }, [updateData]);

  const handleEmailSubmit = useCallback(
    (e) => {
      e.preventDefault();
      sendResetPasswordEmail({
        variables: {
          resetPasswordInfos: {
            email,
          },
        },
      }).catch(() => {});
    },
    [email],
  );

  const handleCodeSubmit = useCallback(
    (value) => {
      setCode(value);
      validateActionCode({
        variables: {
          validateActionCodeInfos: {
            codeId,
            code: value,
          },
        },
      }).catch(() => {});
    },
    [codeId, code],
  );

  const handlePasswordSubmit = useCallback(
    ({ password }) => {
      updateForgotPassword({
        variables: {
          email,
          codeId,
          code,
          password,
        },
      }).catch(() => {});
    },
    [email, codeId, code],
  );

  if (user) return null;

  return (
    <AppLayout>
      <Grid container justifyContent="center">
        <Grid item xs={10} md={4} marginTop={6} marginBottom={6}>
          <Stack direction="column" spacing={1} alignItems="center" marginBottom={3}>
            <Avatar sx={{ backgroundColor: 'secondary.main' }}>
              <VpnKeyOutlined />
            </Avatar>
            <Typography component="h1" variant="h5">
              Mot de passe perdu
            </Typography>
          </Stack>

          {formStep === 'EMAIL_FORM' && (
            <Stack direction="column" spacing={3} alignItems="center" component="form" onSubmit={handleEmailSubmit}>
              <TextField
                fullWidth
                variant="outlined"
                label="Email"
                required
                type="email"
                autoFocus
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Button type="submit" variant="contained" sx={{ margin: 'auto' }}>
                M'envoyer un email de récupération
              </Button>
            </Stack>
          )}

          {formStep === 'CODE_FORM' && (
            <Stack direction="column" spacing={3} alignItems="center" component="form">
              <Typography>Veuillez entrer le code présent dans l'email que vous venez de recevoir :</Typography>
              <CodeField onSubmit={handleCodeSubmit} />
            </Stack>
          )}

          {formStep === 'PASSWORD_FORM' && (
            <UserPasswordForm forgotPassword submitLabel="Valider" onSubmit={handlePasswordSubmit} />
          )}
        </Grid>
      </Grid>
    </AppLayout>
  );
};

export default withApollo()(ForgotPassword);
